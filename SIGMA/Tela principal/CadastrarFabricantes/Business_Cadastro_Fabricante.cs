﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.CadastrarFabricantes
{
    class Business_Cadastro_Fabricante
    {
        public int Salvar (DTO_Cadastrar_Fabricantes dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
                

            }
            if (dto.CNPJ == string.Empty)
            {
                throw new ArgumentException("CNPJ é obrigatório!");
            }
            if (dto.Descricao == string.Empty)
            {
                throw new ArgumentException("Descricão é obrigatório!");
            }
            else
            {
                Database_Cadastrar_Fabricante db = new Database_Cadastrar_Fabricante();
                return db.Salvar(dto);
            }

        }
            
    }
}
