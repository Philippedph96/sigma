﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_Clientes
{
    class DTO_CadastroDeCliente
    {
        public int ID { get; set; } //ok
        public string Nome { get; set; } //ok
        public int CPF { get; set; }//ok
        public string Profissao { get; set; }//ok
        public DateTime DatadeNascimento { get; set; }//ok
        public DateTime ClienteDesde { get; set; }//ok
        public int Sexo { get; set; }//ok
        public string Endereco { get; set; }//ok
        public int CEP { get; set; }//ok
        public string Complemento { get; set; }//ok
        public string Email { get; set; }//ok
        public int Telefone_comercial { get; set; }//ok
        public int Telefone_celular { get; set; }//ok
        public int Telefone_residencial { get; set; }//ok
        public int Numero { get; set; }
      /* Se o cadastro de clientes é somente para pessoa fisica precisa do CNPJ ?
      * public string cnpj { get; set; }//ok */
        public string Tipo_da_carta { get; set; }
        public int RG { get; set; }

    
        //UF é de outra tabela
    }
}
