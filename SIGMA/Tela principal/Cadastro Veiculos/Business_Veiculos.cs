﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_veiculos
{
    class Business_Veiculos
    {
        public int Salvar(DTO_Veiculos dto)
        {
            if (dto.Observacoes == string.Empty)
            {
                throw new ArgumentException("Observações são obrigatórias!");
            }
            if (dto.Combustivel == string.Empty)
            {
                throw new ArgumentException("Combustível é obrigatório!");
            }
            if (dto.Odometro == 0)
            {
                throw new ArgumentException("Odometro é obrigatório!");
            }
            if (dto.Cor == string.Empty)
            {
                throw new ArgumentException("Cor é obrigatório!");
            }
            Database_Veiculos db = new Database_Veiculos();
            return db.Salvar(dto);

        }
        public void Alterar (DTO_Veiculos dto)
        {
            Database_Veiculos db = new Database_Veiculos();
            db.Alterar(dto);
        }
        public void Remover (int id)
        {
            Database_Veiculos db = new Database_Veiculos();
            db.Remover(id);
        }
        public List<DTO_Veiculos> Listar ()
        {
            Database_Veiculos db = new Database_Veiculos();
            List<DTO_Veiculos> list = db.Listar();

            return list;
        }
    }
}
