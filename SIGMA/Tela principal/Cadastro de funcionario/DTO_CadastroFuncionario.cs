﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_funcionario
{
    class DTO_CadastroFuncionario
    {
        public int ID { get; set; } //ok
        public string Nome { get; set; } //ok
        public int CPF { get; set; } //ok

        //Foto tem que ser alterada 
        public  string Foto { get; set; } // ok
        public DateTime DatadeNascimento { get; set; } //ok
        public int Sexo { get; set; } //ok
        public string Endereco { get; set; } //ok
        public int CEP { get; set; } //ok
        public string Cidade { get; set; } //ok
        public string Complemento { get; set; } //ok
        public int Numero { get; set; } //ok
        public string Email { get; set; } //ok
        public int Telefone { get; set; } //ok
        public int Celular { get; set; } //ok
        public int TelefoneResidencial { get; set; } //ok

        //UF,departamento outra tabela 
    }
}
