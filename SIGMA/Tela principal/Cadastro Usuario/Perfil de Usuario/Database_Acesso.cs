﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Login
{
    class Database_Acesso
    {
        public int Salvar (DTO_Acesso dto)
        {
            string script =
            @"INSERT INTO  tb_acesso
            (
                id_usuario,
                bt_acesso_menu,
                bt_inserir_dados,
                bt_alterar_dados,
                bt_excluir_dados
            )
            VALUES
            (
                @id_usuario,
                @bt_acesso_menu,
                @bt_inserir_dados,
                @bt_alterar_dados,
                @bt_excluir_dados
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_usuario", dto.ID_Usuario));
            parm.Add(new MySqlParameter("bt_acesso_menu", dto.Acesso_Menu));
            parm.Add(new MySqlParameter("bt_inserir_dados", dto.Inserir_Dados));
            parm.Add(new MySqlParameter("bt_alterar_dados", dto.Alterar_Dados));
            parm.Add(new MySqlParameter("bt_excluir_dados", dto.Excluir_Dados));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public void Alterar (DTO_Acesso dto)
        {
            string script =
                @"UPDATE tb_acesso
                     SET id_usuario         =   @id_usuario,
                         bt_acesso_menu     =   @bt_acesso_menu,
                         bt_inserir_dados   =   @bt_acesso_menu,
                         bt_alterar_dados   =   @bt_alterar_dados,
                         bt_excluir_dados   =   @bt_excluir_dados
                   WHERE id_acesso          =   @id_acesso";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_acesso", dto.ID));
            parm.Add(new MySqlParameter("bt_acesso_menu", dto.Acesso_Menu));
            parm.Add(new MySqlParameter("bt_inserir_dados", dto.Inserir_Dados));
            parm.Add(new MySqlParameter("bt_alterar_dados", dto.Alterar_Dados));
            parm.Add(new MySqlParameter("bt_excluir_dados", dto.Excluir_Dados));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        public void Remover (int dto)
        {
            string script = @"DELETE FROM tb_acesso
                            WHERE id_acesso = @id_acesso";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_acesso", dto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
    }
}
