﻿namespace Tela_principal.Telas
{
    partial class frm_CadastroDeClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFechar = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblFedelidade = new System.Windows.Forms.Label();
            this.lblFidelidade = new System.Windows.Forms.Label();
            this.lblNascimento = new System.Windows.Forms.Label();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.txtProfissao = new System.Windows.Forms.TextBox();
            this.lblProfissao = new System.Windows.Forms.Label();
            this.rdnJuridica = new System.Windows.Forms.RadioButton();
            this.rdnFisica = new System.Windows.Forms.RadioButton();
            this.rdnOutros = new System.Windows.Forms.RadioButton();
            this.rdnM = new System.Windows.Forms.RadioButton();
            this.rdnF = new System.Windows.Forms.RadioButton();
            this.lblSexo = new System.Windows.Forms.Label();
            this.txtCPF = new System.Windows.Forms.TextBox();
            this.lblCPF = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboUF = new System.Windows.Forms.ComboBox();
            this.lblUF = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.lblCompemento = new System.Windows.Forms.Label();
            this.txtCEP = new System.Windows.Forms.TextBox();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtTelefoneResidencial = new System.Windows.Forms.MaskedTextBox();
            this.lbltxtTelefoneResidencial = new System.Windows.Forms.Label();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.lblCelular = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnInserir = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.Location = new System.Drawing.Point(498, 9);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(17, 17);
            this.lblFechar.TabIndex = 0;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblFedelidade);
            this.panel1.Controls.Add(this.lblFidelidade);
            this.panel1.Controls.Add(this.lblNascimento);
            this.panel1.Controls.Add(this.dtpNascimento);
            this.panel1.Controls.Add(this.txtProfissao);
            this.panel1.Controls.Add(this.lblProfissao);
            this.panel1.Controls.Add(this.rdnJuridica);
            this.panel1.Controls.Add(this.rdnFisica);
            this.panel1.Controls.Add(this.rdnOutros);
            this.panel1.Controls.Add(this.rdnM);
            this.panel1.Controls.Add(this.rdnF);
            this.panel1.Controls.Add(this.lblSexo);
            this.panel1.Controls.Add(this.txtCPF);
            this.panel1.Controls.Add(this.lblCPF);
            this.panel1.Controls.Add(this.txtNome);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblNome);
            this.panel1.Controls.Add(this.lblNumero);
            this.panel1.Controls.Add(this.lblCodigo);
            this.panel1.Location = new System.Drawing.Point(13, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(502, 159);
            this.panel1.TabIndex = 1;
            // 
            // lblFedelidade
            // 
            this.lblFedelidade.AutoSize = true;
            this.lblFedelidade.Location = new System.Drawing.Point(338, 129);
            this.lblFedelidade.Name = "lblFedelidade";
            this.lblFedelidade.Size = new System.Drawing.Size(30, 13);
            this.lblFedelidade.TabIndex = 18;
            this.lblFedelidade.Text = "Data";
            // 
            // lblFidelidade
            // 
            this.lblFidelidade.AutoSize = true;
            this.lblFidelidade.Location = new System.Drawing.Point(255, 129);
            this.lblFidelidade.Name = "lblFidelidade";
            this.lblFidelidade.Size = new System.Drawing.Size(77, 13);
            this.lblFidelidade.TabIndex = 17;
            this.lblFidelidade.Text = "Cliente desde :";
            this.lblFidelidade.Click += new System.EventHandler(this.lblFidelidade_Click);
            // 
            // lblNascimento
            // 
            this.lblNascimento.AutoSize = true;
            this.lblNascimento.Location = new System.Drawing.Point(240, 99);
            this.lblNascimento.Name = "lblNascimento";
            this.lblNascimento.Size = new System.Drawing.Size(107, 13);
            this.lblNascimento.TabIndex = 16;
            this.lblNascimento.Text = "Data de Nascimento:";
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.Location = new System.Drawing.Point(343, 96);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(152, 20);
            this.dtpNascimento.TabIndex = 15;
            // 
            // txtProfissao
            // 
            this.txtProfissao.Location = new System.Drawing.Point(311, 34);
            this.txtProfissao.Name = "txtProfissao";
            this.txtProfissao.Size = new System.Drawing.Size(184, 20);
            this.txtProfissao.TabIndex = 14;
            // 
            // lblProfissao
            // 
            this.lblProfissao.AutoSize = true;
            this.lblProfissao.Location = new System.Drawing.Point(255, 37);
            this.lblProfissao.Name = "lblProfissao";
            this.lblProfissao.Size = new System.Drawing.Size(53, 13);
            this.lblProfissao.TabIndex = 13;
            this.lblProfissao.Text = "Profissão:";
            // 
            // rdnJuridica
            // 
            this.rdnJuridica.AutoSize = true;
            this.rdnJuridica.Location = new System.Drawing.Point(397, 5);
            this.rdnJuridica.Name = "rdnJuridica";
            this.rdnJuridica.Size = new System.Drawing.Size(101, 17);
            this.rdnJuridica.TabIndex = 12;
            this.rdnJuridica.Text = "Pessoa Jurídica";
            this.rdnJuridica.UseVisualStyleBackColor = true;
            // 
            // rdnFisica
            // 
            this.rdnFisica.AutoSize = true;
            this.rdnFisica.Checked = true;
            this.rdnFisica.Location = new System.Drawing.Point(299, 5);
            this.rdnFisica.Name = "rdnFisica";
            this.rdnFisica.Size = new System.Drawing.Size(92, 17);
            this.rdnFisica.TabIndex = 11;
            this.rdnFisica.TabStop = true;
            this.rdnFisica.Text = "Pessoa Física";
            this.rdnFisica.UseVisualStyleBackColor = true;
            // 
            // rdnOutros
            // 
            this.rdnOutros.AutoSize = true;
            this.rdnOutros.Location = new System.Drawing.Point(115, 129);
            this.rdnOutros.Name = "rdnOutros";
            this.rdnOutros.Size = new System.Drawing.Size(51, 17);
            this.rdnOutros.TabIndex = 10;
            this.rdnOutros.Text = "Outro\r\n";
            this.rdnOutros.UseVisualStyleBackColor = true;
            // 
            // rdnM
            // 
            this.rdnM.AutoSize = true;
            this.rdnM.Location = new System.Drawing.Point(84, 129);
            this.rdnM.Name = "rdnM";
            this.rdnM.Size = new System.Drawing.Size(34, 17);
            this.rdnM.TabIndex = 9;
            this.rdnM.Text = "M";
            this.rdnM.UseVisualStyleBackColor = true;
            // 
            // rdnF
            // 
            this.rdnF.AutoSize = true;
            this.rdnF.Location = new System.Drawing.Point(47, 129);
            this.rdnF.Name = "rdnF";
            this.rdnF.Size = new System.Drawing.Size(31, 17);
            this.rdnF.TabIndex = 8;
            this.rdnF.Text = "F";
            this.rdnF.UseVisualStyleBackColor = true;
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.Location = new System.Drawing.Point(3, 129);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(34, 13);
            this.lblSexo.TabIndex = 7;
            this.lblSexo.Text = "Sexo:";
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(46, 64);
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(145, 20);
            this.txtCPF.TabIndex = 6;
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Location = new System.Drawing.Point(3, 64);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(33, 13);
            this.lblCPF.TabIndex = 5;
            this.lblCPF.Text = "CPF :";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(47, 38);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(196, 20);
            this.txtNome.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-4, -4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 22);
            this.label2.TabIndex = 0;
            this.label2.Text = "Cliente";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(3, 41);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(38, 13);
            this.lblNome.TabIndex = 3;
            this.lblNome.Text = "Nome:";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Location = new System.Drawing.Point(49, 22);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(44, 13);
            this.lblNumero.TabIndex = 2;
            this.lblNumero.Text = "Numero\r\n";
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Location = new System.Drawing.Point(3, 22);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(46, 13);
            this.lblCodigo.TabIndex = 1;
            this.lblCodigo.Text = "Código :";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.textBox2);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.cboUF);
            this.panel2.Controls.Add(this.lblUF);
            this.panel2.Controls.Add(this.txtComplemento);
            this.panel2.Controls.Add(this.lblCompemento);
            this.panel2.Controls.Add(this.txtCEP);
            this.panel2.Controls.Add(this.lblEndereco);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.txtEndereco);
            this.panel2.Controls.Add(this.lblCEP);
            this.panel2.Location = new System.Drawing.Point(13, 208);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(502, 119);
            this.panel2.TabIndex = 2;
            // 
            // cboUF
            // 
            this.cboUF.FormattingEnabled = true;
            this.cboUF.Location = new System.Drawing.Point(68, 75);
            this.cboUF.Name = "cboUF";
            this.cboUF.Size = new System.Drawing.Size(121, 21);
            this.cboUF.TabIndex = 26;
            // 
            // lblUF
            // 
            this.lblUF.AutoSize = true;
            this.lblUF.Location = new System.Drawing.Point(38, 78);
            this.lblUF.Name = "lblUF";
            this.lblUF.Size = new System.Drawing.Size(24, 13);
            this.lblUF.TabIndex = 25;
            this.lblUF.Text = "UF:";
            // 
            // txtComplemento
            // 
            this.txtComplemento.Location = new System.Drawing.Point(353, 49);
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(61, 20);
            this.txtComplemento.TabIndex = 22;
            // 
            // lblCompemento
            // 
            this.lblCompemento.AutoSize = true;
            this.lblCompemento.Location = new System.Drawing.Point(270, 52);
            this.lblCompemento.Name = "lblCompemento";
            this.lblCompemento.Size = new System.Drawing.Size(77, 13);
            this.lblCompemento.TabIndex = 21;
            this.lblCompemento.Text = "Complemento :";
            // 
            // txtCEP
            // 
            this.txtCEP.Location = new System.Drawing.Point(68, 49);
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(196, 20);
            this.txtCEP.TabIndex = 20;
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Location = new System.Drawing.Point(3, 26);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(59, 13);
            this.lblEndereco.TabIndex = 8;
            this.lblEndereco.Text = "Endereço :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Modern No. 20", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(-1, -3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 22);
            this.label12.TabIndex = 19;
            this.label12.Text = "Endereço ";
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(68, 23);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(427, 20);
            this.txtEndereco.TabIndex = 7;
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Location = new System.Drawing.Point(28, 52);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(34, 13);
            this.lblCEP.TabIndex = 6;
            this.lblCEP.Text = "CEP :";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtEmail);
            this.panel3.Controls.Add(this.lblEmail);
            this.panel3.Controls.Add(this.txtTelefoneResidencial);
            this.panel3.Controls.Add(this.lbltxtTelefoneResidencial);
            this.panel3.Controls.Add(this.txtCelular);
            this.panel3.Controls.Add(this.lblCelular);
            this.panel3.Controls.Add(this.txtTelefone);
            this.panel3.Controls.Add(this.lblTelefone);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Location = new System.Drawing.Point(13, 344);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(264, 153);
            this.panel3.TabIndex = 3;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(46, 22);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(196, 20);
            this.txtEmail.TabIndex = 27;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(3, 25);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(35, 13);
            this.lblEmail.TabIndex = 33;
            this.lblEmail.Text = "Email:";
            // 
            // txtTelefoneResidencial
            // 
            this.txtTelefoneResidencial.Location = new System.Drawing.Point(68, 111);
            this.txtTelefoneResidencial.Mask = "(999) 000-0000";
            this.txtTelefoneResidencial.Name = "txtTelefoneResidencial";
            this.txtTelefoneResidencial.Size = new System.Drawing.Size(100, 20);
            this.txtTelefoneResidencial.TabIndex = 32;
            // 
            // lbltxtTelefoneResidencial
            // 
            this.lbltxtTelefoneResidencial.AutoSize = true;
            this.lbltxtTelefoneResidencial.Location = new System.Drawing.Point(3, 111);
            this.lbltxtTelefoneResidencial.Name = "lbltxtTelefoneResidencial";
            this.lbltxtTelefoneResidencial.Size = new System.Drawing.Size(65, 26);
            this.lbltxtTelefoneResidencial.TabIndex = 31;
            this.lbltxtTelefoneResidencial.Text = "Telefone \r\nResidencial:";
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(68, 81);
            this.txtCelular.Mask = "(999) 000-0000";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(100, 20);
            this.txtCelular.TabIndex = 30;
            // 
            // lblCelular
            // 
            this.lblCelular.AutoSize = true;
            this.lblCelular.Location = new System.Drawing.Point(3, 84);
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.Size = new System.Drawing.Size(42, 13);
            this.lblCelular.TabIndex = 29;
            this.lblCelular.Text = "Celular:";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(68, 53);
            this.txtTelefone.Mask = "(999) 000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(100, 20);
            this.txtTelefone.TabIndex = 28;
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Location = new System.Drawing.Point(3, 58);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(52, 13);
            this.lblTelefone.TabIndex = 27;
            this.lblTelefone.Text = "Telefone:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Modern No. 20", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(-4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 22);
            this.label18.TabIndex = 27;
            this.label18.Text = "Contato";
            // 
            // btnInserir
            // 
            this.btnInserir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInserir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInserir.Location = new System.Drawing.Point(410, 428);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(75, 23);
            this.btnInserir.TabIndex = 4;
            this.btnInserir.Text = "Inserir";
            this.btnInserir.UseVisualStyleBackColor = true;
            // 
            // btnAlterar
            // 
            this.btnAlterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Location = new System.Drawing.Point(306, 359);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnAlterar.TabIndex = 5;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Location = new System.Drawing.Point(410, 359);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 6;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnExcluir
            // 
            this.btnExcluir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Location = new System.Drawing.Point(306, 395);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 7;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            // 
            // btnSalvar
            // 
            this.btnSalvar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Location = new System.Drawing.Point(306, 428);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 8;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            // 
            // btnFechar
            // 
            this.btnFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Location = new System.Drawing.Point(410, 395);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 9;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "RG:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(46, 92);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(145, 20);
            this.textBox1.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(255, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 26);
            this.label3.TabIndex = 21;
            this.label3.Text = "Tipo de \r\nCarta\r\n";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(311, 62);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(184, 21);
            this.comboBox1.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(420, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "N° :";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(451, 49);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(44, 20);
            this.textBox2.TabIndex = 28;
            // 
            // frm_CadastroDeClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 509);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAlterar);
            this.Controls.Add(this.btnInserir);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblFechar);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_CadastroDeClientes";
            this.Text = "frm_CadastroDeClientes";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.TextBox txtProfissao;
        private System.Windows.Forms.Label lblProfissao;
        private System.Windows.Forms.RadioButton rdnJuridica;
        private System.Windows.Forms.RadioButton rdnFisica;
        private System.Windows.Forms.RadioButton rdnOutros;
        private System.Windows.Forms.RadioButton rdnM;
        private System.Windows.Forms.RadioButton rdnF;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.TextBox txtCPF;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblFedelidade;
        private System.Windows.Forms.Label lblFidelidade;
        private System.Windows.Forms.Label lblNascimento;
        private System.Windows.Forms.ComboBox cboUF;
        private System.Windows.Forms.Label lblUF;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Label lblCompemento;
        private System.Windows.Forms.TextBox txtCEP;
        private System.Windows.Forms.Label lblEndereco;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label lblCEP;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.MaskedTextBox txtTelefoneResidencial;
        private System.Windows.Forms.Label lbltxtTelefoneResidencial;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.Label lblCelular;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnInserir;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
    }
}