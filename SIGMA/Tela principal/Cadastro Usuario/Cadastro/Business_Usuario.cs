﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_Usuario
{
    class Business_Usuario
    {
        public int Salvar(DTO_Usuarios dto)
        {
            if (dto.Nome_User == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }
            if (dto.Obeservacao == string.Empty)
            {
                throw new ArgumentException("É obrigatório escrever uma observação sobre o usuário!");
            }
            if (dto.Perfil == string.Empty)
            {
                throw new ArgumentException("É obrigatório ter um perfil!");
            }
            if (dto.Usuario == string.Empty)
            {
                throw new ArgumentException("É obrigatório ter um Nickname de usuário!");
            }
            if (dto.Senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório!");
            }

            Database_Usuario db = new Database_Usuario();
            return db.Salvar(dto);
        }
        public void Remover(int id)
        {
            Database_Usuario db = new Database_Usuario();
            db.Remover(id);
        }
        public void Alterar(DTO_Usuarios dto)
        {
            Database_Usuario db = new Database_Usuario();
            db.Alterar(dto);
        }
        public bool Logar (DTO_Usuarios dto)
        {
            if (dto.Usuario == string.Empty)
            {
                throw new ArgumentException("Login precisa ser preenchido");
            }
            if (dto.Senha == string.Empty)
            {
                throw new ArgumentException("Senha precisa ser preenchido");
            }

            Database_Usuario db = new Database_Usuario();
            return db.Logar(dto);
        }
        public List<DTO_Usuarios> Listar()
        {
            Database_Usuario db = new Database_Usuario();
            List<DTO_Usuarios> list = db.Listar();

            return list;
        }
        public List<DTO_Usuarios> Consultar(DTO_Usuarios dto)
        {
            Database_Usuario db = new Database_Usuario();
            List<DTO_Usuarios> list = db.Consultar(dto);

            return list;
        }
    }
}
