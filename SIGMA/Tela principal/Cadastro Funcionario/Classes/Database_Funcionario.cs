﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Cadastro_de_funcionario
{
    class Database_Funcionario
    {
        public int Salvar(DTO_Funcionario dto)
        {
            string script =
            @"INSERT INTO tb_funcionario
           (
               id_departamento,
               nm_nome,
               dt_data_nascimento,
               ds_cpf,
               ds_endereco,
               ds_complemento,
               ds_sexo,
               ds_foto,
               ds_cep,
               ds_uf,
               nr_numero_casa,
               ds_email,
               ds_telefone_residencial,
               ds_telefone,
               ds_celular,
               ds_cidade
           )
           VALUES
           (
               @id_departamento,
               @nm_nome,
               @dt_data_nascimento,
               @ds_cpf,
               @ds_endereco,
               @ds_complemento,
               @ds_sexo,
               @ds_foto,
               @ds_cep,
               @ds_uf, 
               @nr_numero_casa,
               @ds_email,
               @ds_telefone_residencial,
               @ds_telefone,
               @ds_celular,
               @ds_cidade
           )";
            //Perceba que dentro do mapeamento que a senhorita fez no DTO existe um campo chamado departamento...
            //Mas esse campo deveria ser chave estrangeira, pois há um relacionamento entre Departamento e funcionário...
            //E não um campo direto!

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_departamento", dto.ID_Departamento));
            parm.Add(new MySqlParameter("nm_nome", dto.Nome));
            parm.Add(new MySqlParameter("dt_data_nascimento", dto.DatadeNascimento));
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parm.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parm.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parm.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parm.Add(new MySqlParameter("ds_foto", dto.Foto));
            parm.Add(new MySqlParameter("ds_cep", dto.CEP));
            parm.Add(new MySqlParameter("ds_uf", dto.UF));
            parm.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parm.Add(new MySqlParameter("nr_numero_casa", dto.Numero));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("ds_telefone_residencial", dto.TelefoneResidencial));
            parm.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parm.Add(new MySqlParameter("ds_celular", dto.Celular));
            
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);

        }

        public void Alterar(DTO_Funcionario dto)
        {
            string script =
                @"UPDATE tb_funcionario
                    SET  id_departamento           = @id_departamento,
                         nm_nome                   = @nm_nome,
                         dt_data_nascimento        = @dt_data_nascimento,
                         ds_cpf                    = @ds_cpf,
                         ds_endereco               = @ds_endereco,
                         ds_complemento            = @ds_complemento,
                         ds_sexo                   = @ds_sexo,
                         ds_foto                   = @ds_foto,
                         ds_cep                    = @ds_cep,
                         ds_uf                     = @ds_uf 
                         nr_numero_casa            = @nr_numero_casa,
                         ds_email                  = @ds_email,
                         ds_telefone_residencial   = @ds_telefone_residencial,
                         ds_telefone               = @ds_telefone,
                         ds_celular                = @ds_celular,
                         ds_cidade                 = @ds_cidade
                  WHERE id_funcionario             = @id_funcionario";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_departamento", dto.ID_Departamento));
            parm.Add(new MySqlParameter("nm_nome", dto.Nome));
            parm.Add(new MySqlParameter("dt_data_nascimento", dto.DatadeNascimento));
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parm.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parm.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parm.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parm.Add(new MySqlParameter("ds_foto", dto.Foto));
            parm.Add(new MySqlParameter("ds_cep", dto.CEP));
            parm.Add(new MySqlParameter("ds_uf", dto.UF));
            parm.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parm.Add(new MySqlParameter("nr_numero_casa", dto.Numero));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("ds_telefone_residencial", dto.TelefoneResidencial));
            parm.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parm.Add(new MySqlParameter("ds_celular", dto.Celular));


            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        public void Remover(int dto)
        {
            string script =
                @"DELETE FROM tb_funcionario
                       WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_funcionario", dto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }

        public List<DTO_Funcionario> Listar()
        {
            string script = "SELECT * FROM tb_funcionario";

            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Funcionario> fora = new List<DTO_Funcionario>();

            while (reader.Read())
            {
                DTO_Funcionario dentro = new DTO_Funcionario();
                dentro.ID = reader.GetInt32("id_funcionario");
                dentro.ID_Departamento = reader.GetInt32("id_departamento");
                dentro.Nome = reader.GetString("nm_nome");
                dentro.DatadeNascimento = reader.GetDateTime("dt_data_nascimento");
                dentro.CPF = reader.GetString("ds_cpf");
                dentro.Endereco = reader.GetString("ds_endereco");
                dentro.Complemento = reader.GetString("ds_complemento");
                dentro.Sexo = reader.GetBoolean("ds_sexo");
                dentro.CEP = reader.GetString("ds_cep");
                dentro.UF = reader.GetString("ds_uf");
                dentro.Numero = reader.GetInt32("nr_numero_casa");
                dentro.Email = reader.GetString("ds_email");
                dentro.TelefoneResidencial = reader.GetString("ds_telefone_residencial");
                dentro.Telefone = reader.GetString("ds_telefone");
                dentro.Celular = reader.GetString("ds_celular");
                dentro.Cidade= reader.GetString("ds_cidade");

                fora.Add(dentro);
            }

            reader.Close();
            return fora;
        }
    }
}
