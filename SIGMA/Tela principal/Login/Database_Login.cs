﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Login
{
    class Database_Login
    {
        public int Salvar (DTO_Login dto)
        {
            string script =
            @"INSERT INTO tb_usuario
            (
                nm_nome,
                nm_usuario,
                ds_senha,
                ds_status,
                ds_observacao
            )
            VALUES
            (
                nm_nome,
                nm_usuario,
                nr_senha,
                ds_status,
                ds_observacao
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_nome", dto.Nome));
            parm.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));
            parm.Add(new MySqlParameter("ds_status", dto.Status));
            parm.Add(new MySqlParameter("ds_observacao", dto.Observacao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        //Esse método só será utilizado caso tenho o campo de mudar de senha!
        //public void Alterar (DTO_Login dto)
        //{
            //string script =
                //@"UPDATE tb_login
                     //SET nm_nome        = @nm_nome,
                         //nm_usuario     = @nm_usuario,
                         //ds_senha       = @ds_senha,
                         //ds_status      = @ds_status,
                         //ds_observacao  = @ds_observacao
                   //WHERE id_usuario     = @id_usuario";

            //List<MySqlParameter> parm = new List<MySqlParameter>();
            //parm.Add(new MySqlParameter("id_usuario", dto.ID));

            //parm.Add(new MySqlParameter("nm_nome", dto.Nome));
            //parm.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            //parm.Add(new MySqlParameter("ds_senha", dto.Senha));
            //parm.Add(new MySqlParameter("ds_status", dto.Status));
            //parm.Add(new MySqlParameter("ds_observacao", dto.Observacao));

            //Database db = new Database();
            //db.ExecuteInsertScript(script, parm);
        //}

        public bool Logar (DTO_Login dto)
        {
            string script =
                @"SELECT * FROM tb_usuario
                   WHERE nm_usuario = @nm_usuario
                     AND ds_senha   = @ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
