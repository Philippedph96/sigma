﻿namespace Tela_principal.Cadastro_de_veiculos
{
    partial class frm_CadastroDeVeiculos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFechar = new System.Windows.Forms.Label();
            this.txtPlaca = new System.Windows.Forms.TextBox();
            this.lblPlaca = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNumeroCodigo = new System.Windows.Forms.Label();
            this.lblProprietario = new System.Windows.Forms.Label();
            this.lblNomeProp = new System.Windows.Forms.Label();
            this.lblModelo = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblObservacao = new System.Windows.Forms.Label();
            this.rtbObservacao = new System.Windows.Forms.RichTextBox();
            this.lblCombustivel = new System.Windows.Forms.Label();
            this.cboCombustivel = new System.Windows.Forms.ComboBox();
            this.lblOdometro = new System.Windows.Forms.Label();
            this.txtOdometro = new System.Windows.Forms.TextBox();
            this.lblCor = new System.Windows.Forms.Label();
            this.txtCor = new System.Windows.Forms.TextBox();
            this.lblAnoFabricacao = new System.Windows.Forms.Label();
            this.cboAnoFabricacao = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.Location = new System.Drawing.Point(488, 9);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(17, 17);
            this.lblFechar.TabIndex = 16;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // txtPlaca
            // 
            this.txtPlaca.Location = new System.Drawing.Point(58, 62);
            this.txtPlaca.Name = "txtPlaca";
            this.txtPlaca.Size = new System.Drawing.Size(101, 20);
            this.txtPlaca.TabIndex = 15;
            // 
            // lblPlaca
            // 
            this.lblPlaca.AutoSize = true;
            this.lblPlaca.Location = new System.Drawing.Point(12, 62);
            this.lblPlaca.Name = "lblPlaca";
            this.lblPlaca.Size = new System.Drawing.Size(37, 13);
            this.lblPlaca.TabIndex = 14;
            this.lblPlaca.Text = "Placa:";
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Location = new System.Drawing.Point(12, 43);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(43, 13);
            this.lblCodigo.TabIndex = 12;
            this.lblCodigo.Text = "Código:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Modern No. 20", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 24);
            this.label3.TabIndex = 11;
            this.label3.Text = "Dados Gerais";
            // 
            // lblNumeroCodigo
            // 
            this.lblNumeroCodigo.AutoSize = true;
            this.lblNumeroCodigo.Location = new System.Drawing.Point(64, 43);
            this.lblNumeroCodigo.Name = "lblNumeroCodigo";
            this.lblNumeroCodigo.Size = new System.Drawing.Size(46, 13);
            this.lblNumeroCodigo.TabIndex = 17;
            this.lblNumeroCodigo.Text = "Código :";
            // 
            // lblProprietario
            // 
            this.lblProprietario.AutoSize = true;
            this.lblProprietario.Location = new System.Drawing.Point(12, 96);
            this.lblProprietario.Name = "lblProprietario";
            this.lblProprietario.Size = new System.Drawing.Size(63, 13);
            this.lblProprietario.TabIndex = 18;
            this.lblProprietario.Text = "Proprietário:";
            // 
            // lblNomeProp
            // 
            this.lblNomeProp.AutoSize = true;
            this.lblNomeProp.Location = new System.Drawing.Point(84, 96);
            this.lblNomeProp.Name = "lblNomeProp";
            this.lblNomeProp.Size = new System.Drawing.Size(33, 13);
            this.lblNomeProp.TabIndex = 19;
            this.lblNomeProp.Text = "nome";
            // 
            // lblModelo
            // 
            this.lblModelo.AutoSize = true;
            this.lblModelo.Location = new System.Drawing.Point(30, 120);
            this.lblModelo.Name = "lblModelo";
            this.lblModelo.Size = new System.Drawing.Size(45, 13);
            this.lblModelo.TabIndex = 20;
            this.lblModelo.Text = "Modelo:";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(84, 120);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(33, 13);
            this.lblModel.TabIndex = 21;
            this.lblModel.Text = "nome";
            // 
            // lblObservacao
            // 
            this.lblObservacao.AutoSize = true;
            this.lblObservacao.Location = new System.Drawing.Point(7, 143);
            this.lblObservacao.Name = "lblObservacao";
            this.lblObservacao.Size = new System.Drawing.Size(73, 13);
            this.lblObservacao.TabIndex = 22;
            this.lblObservacao.Text = "Observações:";
            // 
            // rtbObservacao
            // 
            this.rtbObservacao.Location = new System.Drawing.Point(12, 159);
            this.rtbObservacao.Name = "rtbObservacao";
            this.rtbObservacao.Size = new System.Drawing.Size(460, 96);
            this.rtbObservacao.TabIndex = 23;
            this.rtbObservacao.Text = "";
            // 
            // lblCombustivel
            // 
            this.lblCombustivel.AutoSize = true;
            this.lblCombustivel.Location = new System.Drawing.Point(275, 43);
            this.lblCombustivel.Name = "lblCombustivel";
            this.lblCombustivel.Size = new System.Drawing.Size(67, 13);
            this.lblCombustivel.TabIndex = 24;
            this.lblCombustivel.Text = "Combustivel:";
            // 
            // cboCombustivel
            // 
            this.cboCombustivel.FormattingEnabled = true;
            this.cboCombustivel.Location = new System.Drawing.Point(351, 40);
            this.cboCombustivel.Name = "cboCombustivel";
            this.cboCombustivel.Size = new System.Drawing.Size(121, 21);
            this.cboCombustivel.TabIndex = 25;
            // 
            // lblOdometro
            // 
            this.lblOdometro.AutoSize = true;
            this.lblOdometro.Location = new System.Drawing.Point(275, 69);
            this.lblOdometro.Name = "lblOdometro";
            this.lblOdometro.Size = new System.Drawing.Size(56, 13);
            this.lblOdometro.TabIndex = 26;
            this.lblOdometro.Text = "Odômetro:";
            // 
            // txtOdometro
            // 
            this.txtOdometro.Location = new System.Drawing.Point(351, 69);
            this.txtOdometro.Name = "txtOdometro";
            this.txtOdometro.Size = new System.Drawing.Size(101, 20);
            this.txtOdometro.TabIndex = 27;
            // 
            // lblCor
            // 
            this.lblCor.AutoSize = true;
            this.lblCor.Location = new System.Drawing.Point(308, 96);
            this.lblCor.Name = "lblCor";
            this.lblCor.Size = new System.Drawing.Size(26, 13);
            this.lblCor.TabIndex = 28;
            this.lblCor.Text = "Cor:";
            // 
            // txtCor
            // 
            this.txtCor.Location = new System.Drawing.Point(351, 96);
            this.txtCor.Name = "txtCor";
            this.txtCor.Size = new System.Drawing.Size(101, 20);
            this.txtCor.TabIndex = 29;
            // 
            // lblAnoFabricacao
            // 
            this.lblAnoFabricacao.AutoSize = true;
            this.lblAnoFabricacao.Location = new System.Drawing.Point(282, 120);
            this.lblAnoFabricacao.Name = "lblAnoFabricacao";
            this.lblAnoFabricacao.Size = new System.Drawing.Size(63, 26);
            this.lblAnoFabricacao.TabIndex = 30;
            this.lblAnoFabricacao.Text = "Ano de \r\nFabricação:\r\n";
            // 
            // cboAnoFabricacao
            // 
            this.cboAnoFabricacao.FormattingEnabled = true;
            this.cboAnoFabricacao.Location = new System.Drawing.Point(351, 125);
            this.cboAnoFabricacao.Name = "cboAnoFabricacao";
            this.cboAnoFabricacao.Size = new System.Drawing.Size(82, 21);
            this.cboAnoFabricacao.TabIndex = 31;
            // 
            // frm_CadastroDeVeiculos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 277);
            this.Controls.Add(this.cboAnoFabricacao);
            this.Controls.Add(this.lblAnoFabricacao);
            this.Controls.Add(this.txtCor);
            this.Controls.Add(this.lblCor);
            this.Controls.Add(this.txtOdometro);
            this.Controls.Add(this.lblOdometro);
            this.Controls.Add(this.cboCombustivel);
            this.Controls.Add(this.lblCombustivel);
            this.Controls.Add(this.rtbObservacao);
            this.Controls.Add(this.lblObservacao);
            this.Controls.Add(this.lblModel);
            this.Controls.Add(this.lblModelo);
            this.Controls.Add(this.lblNomeProp);
            this.Controls.Add(this.lblProprietario);
            this.Controls.Add(this.lblNumeroCodigo);
            this.Controls.Add(this.lblFechar);
            this.Controls.Add(this.txtPlaca);
            this.Controls.Add(this.lblPlaca);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.label3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_CadastroDeVeiculos";
            this.Text = "frm_CadastroDeVeiculos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.TextBox txtPlaca;
        private System.Windows.Forms.Label lblPlaca;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblNumeroCodigo;
        private System.Windows.Forms.Label lblProprietario;
        private System.Windows.Forms.Label lblNomeProp;
        private System.Windows.Forms.Label lblModelo;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblObservacao;
        private System.Windows.Forms.RichTextBox rtbObservacao;
        private System.Windows.Forms.Label lblCombustivel;
        private System.Windows.Forms.ComboBox cboCombustivel;
        private System.Windows.Forms.Label lblOdometro;
        private System.Windows.Forms.TextBox txtOdometro;
        private System.Windows.Forms.Label lblCor;
        private System.Windows.Forms.TextBox txtCor;
        private System.Windows.Forms.Label lblAnoFabricacao;
        private System.Windows.Forms.ComboBox cboAnoFabricacao;
    }
}