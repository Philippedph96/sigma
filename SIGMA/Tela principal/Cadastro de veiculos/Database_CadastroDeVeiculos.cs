﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Cadastro_de_veiculos
{
    class Database_CadastroDeVeiculos
    {
        public int Salvar(DTO_CadastroDeVeiculos dto)
        {
            string script =
            @"INSERT INTO tb_veiculos
            (
                ds_plca,
                dt_ano,
                ds_cor,
                ds_odometro,
                ds_combustivel,
                ds_observacoes
            ) 
            VALUES
            (
                @ds_plca,
                @dt_ano,
                @ds_cor,
                @ds_odometro,
                @ds_combustivel,
                @ds_observacoes
             )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_placa", dto.Placa));
            parm.Add(new MySqlParameter("dt_ano", dto.Ano));
            parm.Add(new MySqlParameter("ds_cor", dto.Cor));
            parm.Add(new MySqlParameter("ds_odometro", dto.Odometro));
            parm.Add(new MySqlParameter("ds_combustivel", dto.Combustivel));
            parm.Add(new MySqlParameter("ds_observacoes", dto.Observaçoes));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public void Alterar (DTO_CadastroDeVeiculos dto)
        {
            string script =
                @"UPDATE tb_veiculo
                     SET ds_plca            = @ds_plca,
                         dt_ano             = @dt_ano,
                         ds_cor             = @ds_cor,
                         ds_odometro        = @ds_odometro,
                         ds_combustivel     = @ds_combustivel,
                         ds_observacoes     = @ds_observacoes
                     WHERE id_veiculos      = @id_veiculos";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_placa", dto.Placa));
            parm.Add(new MySqlParameter("dt_ano", dto.Ano));
            parm.Add(new MySqlParameter("ds_odometro", dto.Odometro));
            parm.Add(new MySqlParameter("ds_combustivel", dto.Combustivel));
            parm.Add(new MySqlParameter("ds_observacoes", dto.Observaçoes));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }

        public void Remover(int dto)
        {
            string script =
                @"DELETE FROM tb_veiculo
                       WHERE id_veiculo = @id_veiculo";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_veiculo", dto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
    }
}
