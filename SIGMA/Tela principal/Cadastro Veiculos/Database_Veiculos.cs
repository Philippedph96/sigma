﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Cadastro_de_veiculos
{
    class Database_Veiculos
    {
        public int Salvar(DTO_Veiculos dto)
        {
            string script =
            @"INSERT INTO tb_veiculo
            (
                id_cliente,
                id_modelo,
                ds_placa,
                ds_cor,
                ds_observacao,
                ds_combustivel,
                ds_odometro,
                dt_ano
            ) 
            VALUES
            (
                @id_cliente,
                @id_modelo,
                @ds_placa,
                @ds_cor,
                @ds_observacao,
                @ds_combustivel,
                @ds_odometro,
                @dt_ano
             )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_cliente", dto.ID_Cliente));
            parm.Add(new MySqlParameter("id_modelo", dto.ID_Modelo));
            parm.Add(new MySqlParameter("ds_placa", dto.Placa));
            parm.Add(new MySqlParameter("ds_cor", dto.Cor));
            parm.Add(new MySqlParameter("ds_observacao", dto.Observacoes));
            parm.Add(new MySqlParameter("ds_combustivel", dto.Combustivel));
            parm.Add(new MySqlParameter("ds_odometro", dto.Odometro));
            parm.Add(new MySqlParameter("dt_ano", dto.Ano));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public void Alterar (DTO_Veiculos dto)
        {
            string script =
                @"UPDATE tb_veiculo
                     SET id_cliente       = @id_cliente,
                         id_modelo        = @id_modelo,
                         ds_placa         = @ds_placa,
                         ds_cor           = @ds_cor,
                         ds_observacao    = @ds_observacao,
                         ds_combustivel   = @ds_combustivel,
                         ds_odometro      = @ds_odometro,
                         dt_ano           = @dt_ano
                   WHERE id_veiculos      = @id_veiculos";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_cliente", dto.ID_Cliente));
            parm.Add(new MySqlParameter("id_modelo", dto.ID_Modelo));
            parm.Add(new MySqlParameter("ds_placa", dto.Placa));
            parm.Add(new MySqlParameter("ds_cor", dto.Cor));
            parm.Add(new MySqlParameter("ds_observacao", dto.Observacoes));
            parm.Add(new MySqlParameter("ds_combustivel", dto.Combustivel));
            parm.Add(new MySqlParameter("ds_odometro", dto.Odometro));
            parm.Add(new MySqlParameter("dt_ano", dto.Ano));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }

        public void Remover(int dto)
        {
            string script =
                @"DELETE FROM tb_veiculo
                       WHERE id_veiculo = @id_veiculo";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_veiculo", dto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }

        public List<DTO_Veiculos> Listar ()
        {
            string script = @"SELECT * FROM tb_veiculo";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Veiculos> fora = new List<DTO_Veiculos>();

            while (reader.Read())
            {
                DTO_Veiculos dentro = new DTO_Veiculos();
                dentro.ID = reader.GetInt32("id_veiculo");
                dentro.ID_Cliente = reader.GetInt32("id_cliente");
                dentro.ID_Modelo = reader.GetInt32("id_modelo");
                dentro.Placa = reader.GetString("ds_placa");
                dentro.Cor = reader.GetString("ds_cor");
                dentro.Observacoes = reader.GetString("ds_observacao");
                dentro.Combustivel = reader.GetString("ds_combustivel");
                dentro.Odometro = reader.GetInt32("ds_odometro");
                dentro.Ano = reader.GetString("dt_ano");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
    }
}
