﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Cadastro_Usuario
{
    class Database_Departamento
    {
        public List<DTO_Departamento> Listar()
        {
            string script = @"SELECT * FROM tb_departamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Departamento> lista = new List<DTO_Departamento>();
            while(reader.Read())
            {
                DTO_Departamento dto = new DTO_Departamento();
                dto.ID = reader.GetInt32("id_departamento");
                dto.Departamento = reader.GetString("nm_departamento");
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        
    }
}
