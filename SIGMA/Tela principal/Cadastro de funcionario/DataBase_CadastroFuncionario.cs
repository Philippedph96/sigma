﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_funcionario
{
    class DataBase_CadastroFuncionario
    {
        public int Salvar(DTO_CadastroFuncionario dto)
        {
            string script =
            @"INSERT INTO tb_funcionario
           (
               nm_nome,
               ds_cpf,
               ds_departamento,
               ds_foto,
               dt_nascimento,
               ds_sexo,
               ds_endereco,
               nr_cep,
               ds_cidade,
               ds_complemento,
               nr_numero,
               ds_email,
               nr_telefone,
               nr_celular,
               nr_telefone_residencial
           )
           VALUES
           (
               @nm_nome,
               @ds_cpf,
               @ds_departamento,
               @ds_foto,
               @dt_nascimento,
               @ds_sexo,
               @ds_endereco,
               @nr_cep,
               @ds_cidade,
               @ds_complemento,
               @nr_numero,
               @ds_email,
               @nr_telefone,
               @nr_celular,
               @nr_telefone_residencial
           )";
            //Perceba que dentro do mapeamento que a senhorita fez no DTO existe um campo chamado departamento...
            //Mas esse campo deveria ser chave estrangeira, pois há um relacionamento entre Departamento e funcionário...
            //E não um campo direto!

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_nome", dto.Nome));
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parm.Add(new MySqlParameter("ds_foto", dto.Foto));
            parm.Add(new MySqlParameter("dt_nascimento", dto.DatadeNascimento));
            parm.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parm.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parm.Add(new MySqlParameter("nr_cep", dto.CEP));
            parm.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parm.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parm.Add(new MySqlParameter("nr_numero", dto.Numero));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("nr_telefone", dto.Telefone));
            parm.Add(new MySqlParameter("nr_celular", dto.Celular));
            parm.Add(new MySqlParameter("nr_telefone_residencial", dto.TelefoneResidencial));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);

        }

        public void Alterar(DTO_CadastroFuncionario dto)
        {
            string script =
                @"UPDATE tb_funcionario
                    SET nm_nome                    = @nm_nome,
                        ds_cpf                     = @ds_cpf,
                        ds_departamento            = @ds_departamento,
                        ds_foto                    = @ds_foto,
                        dt_nascimento              = @dt_nascimento,
                        ds_sexo                    = @ds_sexo,
                        ds_endereco                = @ds_endereco,
                        nr_cep                     = @nr_cep,
                        ds_cidade                  = @ds_cidade,
                        ds_complemento             = @ds_complemento,
                        nr_numero                  = @nr_numero,
                        ds_email                   = @ds_email,
                        nr_telefone                = @nr_telefone,
                        nr_celular                 = @nr_celular,
                        nr_telefone_residencial    = @nr_telefone_residencial
                  WHERE id_funcionario             = @id_funcionario";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_funcionario", dto.ID));
            parm.Add(new MySqlParameter("nm_nome", dto.Nome));
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parm.Add(new MySqlParameter("ds_foto", dto.Foto));
            parm.Add(new MySqlParameter("dt_nascimento", dto.DatadeNascimento));
            parm.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parm.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parm.Add(new MySqlParameter("nr_cep", dto.CEP));
            parm.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parm.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parm.Add(new MySqlParameter("nr_numero", dto.Numero));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("nr_telefone", dto.Telefone));
            parm.Add(new MySqlParameter("nr_celular", dto.Celular));
            parm.Add(new MySqlParameter("nr_telefone_residencial", dto.TelefoneResidencial));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        public void Remover(int dto)
        {
            string script =
                @"DELETE FROM tb_funcionario
                       WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_funcionario", dto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
    }
}
