﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tela_principal.Telas
{
    public partial class frm_CadastroDeClientes : Form
    {
        public frm_CadastroDeClientes()
        {
            InitializeComponent();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar este programa?",
                                            "Projeto Sigma",
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void lblFidelidade_Click(object sender, EventArgs e)
        {

        }
    }
}
