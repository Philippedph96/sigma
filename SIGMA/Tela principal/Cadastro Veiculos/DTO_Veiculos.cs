﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_veiculos
{
    class DTO_Veiculos
    {
        public int ID { get; set; } //ok
        public int ID_Cliente { get; set; } //ok
        public int ID_Modelo { get; set; } //ok
        public string Placa { get; set; } //ok
        public string Combustivel { get; set; } //ok
        public int Odometro { get; set; } //ok
        public string Cor { get; set; } //ok
        public string Ano { get; set; } //ok
        public string Observacoes { get; set; } //ok
        //proprietario e modelo , outra tabela 

    }
}
