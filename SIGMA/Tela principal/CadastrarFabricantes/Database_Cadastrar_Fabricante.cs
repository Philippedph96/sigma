﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.CadastrarFabricantes
{
    class Database_Cadastrar_Fabricante
    {
        public int Salvar (DTO_Cadastrar_Fabricantes dto)
        {
            string script = @"INSERT INTO tb_fabricante
                              (nm_nome,ds_cnpj,ds_descricao)
                                VALUES
                                  (@nm_nome,@ds_cnpj,@ds_descricao) ";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_nome",dto.Nome));
            parm.Add(new MySqlParameter("ds_cnpj", dto.CNPJ));
            parm.Add(new MySqlParameter("ds_descricao", dto.Descricao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);

        }
    }
}
