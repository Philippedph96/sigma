﻿using Correios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Banco.Plugin;
using Tela_principal.Cadastro_de_funcionario;
using Tela_principal.Cadastro_Usuario;

namespace Tela_principal.Orçamento
{
    public partial class frmCadastrar_Funcionario : Form
    {
        public frmCadastrar_Funcionario()
        {
            InitializeComponent();
            CarregarCombos();

            List<string> uf = new List<string>();
            uf.Add("AC");
            uf.Add("AL");
            uf.Add("AM");
            uf.Add("AP");
            uf.Add("BA");
            uf.Add("CE");
            uf.Add("DF");
            uf.Add("ES");
            uf.Add("GO");
            uf.Add("MA");
            uf.Add("MG");
            uf.Add("MS");
            uf.Add("MT");
            uf.Add("PA");
            uf.Add("PB");
            uf.Add("PE");
            uf.Add("PI");
            uf.Add("PR");
            uf.Add("RJ");
            uf.Add("RN");
            uf.Add("RO");
            uf.Add("RR");
            uf.Add("RS");
            uf.Add("SC");
            uf.Add("SE");
            uf.Add("SP");
            uf.Add("TO");

            cboUF.DataSource = uf;
        }

        
        public void CarregarCombos()
        {
            try
            {
                Business_Departamento db = new Business_Departamento();
                List<DTO_Departamento> list = db.Listar();

                cboDepartamento.ValueMember = nameof(DTO_Departamento.ID);
                cboDepartamento.DisplayMember = nameof(DTO_Departamento.Departamento);

                cboDepartamento.DataSource = list;
            }
            
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!\n " + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        public bool VerificarCPF(string cpf)
        {
            try
            {
                if (txtCPF.Text.Length == 14)
                {
                    int n1 = int.Parse(txtCPF.Text.Substring(0, 1));
                    int n2 = int.Parse(txtCPF.Text.Substring(1, 1));
                    int n3 = int.Parse(txtCPF.Text.Substring(2, 1));
                    int n4 = int.Parse(txtCPF.Text.Substring(4, 1));
                    int n5 = int.Parse(txtCPF.Text.Substring(5, 1));
                    int n6 = int.Parse(txtCPF.Text.Substring(6, 1));
                    int n7 = int.Parse(txtCPF.Text.Substring(8, 1));
                    int n8 = int.Parse(txtCPF.Text.Substring(9, 1));
                    int n9 = int.Parse(txtCPF.Text.Substring(10, 1));

                    int digito1 = int.Parse(txtCPF.Text.Substring(12, 1));
                    int digito2 = int.Parse(txtCPF.Text.Substring(13, 1));

                    //Está verificando se os números são iguais. Se forem, o número colocado é burlado
                    if (n1 == n2 && n2 == n3 && n3 == n4 && n4 == n5 && n5 == n6 && n6 == n7 && n7 == n8 && n8 == n9)
                    {
                        return false;
                    }

                    // Aqui está sendo multiplicado e somado todos os números que o usuário digitou em uma sequência de 10 à 2.
                    int soma1 = n1 * 10 + n2 * 9 + n3 * 8 + n4 * 7 + n5 * 6 + n6 * 5 + n7 * 4 + n8 * 3 + n9 * 2;

                    // Logo após, é pego o total da soma e é dividido por 11 pegando o RESTO dessa divisão. 
                    int digitoverificador1 = soma1 % 11;

                    // Verificar se o valor é menor ou maior que 2
                    if (digitoverificador1 < 2)
                    {
                        digitoverificador1 = 0;
                    }
                    else
                    {
                        digitoverificador1 = 11 - digitoverificador1;
                    }

                    // Agora, basicamente, é feito o mesmo processo...
                    // Soma e multiplicar todos os números em uma sequência de agora 11 números
                    int soma2 = n1 * 11 + n2 * 10 + n3 * 9 + n4 * 8 + n5 * 7 + n6 * 6 + n7 * 5 + n8 * 4 + n9 * 3 + digitoverificador1 * 2;

                    // Logo após, é pego o total da soma e é dividido por 11 pegando o RESTO dessa divisão. 
                    int digitoverificador2 = soma2 % 11;

                    // Verificar se o valor é menor ou maior que 2
                    if (digitoverificador1 < 2)
                    {
                        digitoverificador1 = 0;
                    }
                    else
                    {
                        digitoverificador1 = 11 - digitoverificador1;
                    }

                    // Verificar se os dois dígitos são iguais as digitados pelo usuário
                    if (digito1 == digitoverificador1 && digito2 == digitoverificador2)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }
        public void Automagicamente()
        {
            try
            {

            DTO_Departamento departamento = cboDepartamento.SelectedItem as DTO_Departamento;

            DTO_Funcionario dto = new DTO_Funcionario();
            dto.Nome = txtNome.Text;
            dto.CPF = txtCPF.Text;

            if (VerificarCPF(dto.CPF) == false)
            {
                MessageBox.Show("CPF inválido!" + "\n" + "Verifique se o CPF está digitalizado da maneira correta",
                           "Sigma",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error);
                return;
            }

            if (rdnF.Checked == true)
            {
                dto.Sexo = rdnF.Checked;
            }
            else
            {
                dto.Sexo = rdnM.Checked;
            }
            dto.DatadeNascimento = dtpNascimento.Value;
            dto.Endereco = txtEndereco.Text;
            dto.CEP = txtCEP.Text;
            dto.UF = cboUF.SelectedItem.ToString();
            dto.Complemento = txtComplemento.Text;
            dto.Numero = int.Parse(txtNumero.Text);
            dto.Cidade = txtCidade.Text;
            dto.Email = txtEmail.Text;
            dto.Telefone = txtTelefone.Text;
            dto.TelefoneResidencial = txtTelefoneResidencial.Text;
            dto.Celular = txtCelular.Text;
            dto.Foto = ImagemPlugin.ConverterParaString(imgFoto.Image);
            dto.ID_Departamento = departamento.ID;

            Business_Funcionario db = new Business_Funcionario();
            db.Salvar(dto);

            MessageBox.Show("Cliente cadastrado com sucesso",
                              "Sigma",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        public void Abracadabra ()
    
        {
            try
            {

                DTO_Departamento departamento = cboDepartamento.SelectedItem as DTO_Departamento;

                DTO_Funcionario dto = new DTO_Funcionario();
                dto.Nome = txtNome.Text;
                dto.CPF = txtCPF.Text;
                if (rdnF.Checked == true)
                {
                    dto.Sexo = rdnF.Checked;
                }
                else
                {
                    dto.Sexo = rdnM.Checked;
                }
                dto.DatadeNascimento = dtpNascimento.Value;
                dto.Endereco = txtEndereco.Text;
                dto.CEP = txtCEP.Text;
                dto.UF = cboUF.SelectedItem.ToString();
                dto.Complemento = txtComplemento.Text;
                dto.Numero = int.Parse(txtNumero.Text);
                dto.Cidade = txtCidade.Text;
                dto.Email = txtEmail.Text;
                dto.Telefone = txtTelefone.Text;
                dto.TelefoneResidencial = txtTelefoneResidencial.Text;
                dto.Celular = txtCelular.Text;
                dto.Foto = ImagemPlugin.ConverterParaString(imgFoto.Image);
                dto.ID_Departamento = departamento.ID;

                Business_Funcionario db = new Business_Funcionario();
                db.Alterar(dto);

                MessageBox.Show("Cliente cadastrado com sucesso",
                                  "Sigma",
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                               "Sigma",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        public void Magicamente()
        {
            try
            {
                DialogResult dialog = MessageBox.Show("Você tem certeza que deseja cancelar o cadastro?",
                                                  "Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

                if (dialog == DialogResult.Yes)
                {
                    this.Hide();

                    frmTela_Principal tela = new frmTela_Principal();
                    tela.Show();
                }

            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }
        public void Alacazum()
        {
            try
            {

                DialogResult dialog = MessageBox.Show("Você realmente deseja deletar esse registro?",
                                "Sigma",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question);

                if (dialog == DialogResult.Yes)
                {
                    DTO_Funcionario dto = new DTO_Funcionario();
                    int id = dto.ID;

                    Business_Funcionario db = new Business_Funcionario();
                    db.Remover(id);
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }

        }
        public void Desaparecer ()
        {
            try
            {

                DialogResult dialog = MessageBox.Show("Deseja realmente fechar este programa?",
                                                            "Projeto Sigma",
                                                            MessageBoxButtons.YesNo,
                                                            MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    frmTela_Inicializacao tela = new frmTela_Inicializacao();
                    tela.Show();
                    this.Close();
                }

            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }
        public void SHAZAM ()
        {
            txtNome.Enabled = true;
            txtCPF.Enabled = true;
            rdnF.Enabled = true;
            rdnM.Enabled = true;
            cboDepartamento.Enabled = true;
            dtpNascimento.Enabled = true;
            btnCaminho.Enabled = true;
            txtEndereco.Enabled = true;
            txtCEP.Enabled = true;
            cboUF.Enabled = true;
            txtNumero.Enabled = true;            
            txtComplemento.Enabled = true;
            txtCidade.Enabled = true;
            txtEmail.Enabled = true;
            txtTelefone.Enabled = true;
            txtTelefoneResidencial.Enabled = true;
            txtCelular.Enabled = true;
            btnAlterar.Enabled = true;
            btnCancelar.Enabled = true;
            btnExcluir.Enabled = true;
            btnSalvar.Enabled = true;
        }

        private void btnCaminho_Click(object sender, EventArgs e)
        {
            try
            {

            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgFoto.ImageLocation = dialog.FileName;
            }
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void label23_Click(object sender, EventArgs e)
        {
            Desaparecer();
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            SHAZAM();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Desaparecer();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Abracadabra();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Magicamente();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Alacazum();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Automagicamente();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
            this.Close();
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Esse campo aceita apenas Números",
                                "SIGMA",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CorreiosApi service = new CorreiosApi();
                var cep = txtCEP.Text;
                var dados = service.consultaCEP(cep);

                string rua = dados.end;
                string bairro = dados.bairro;
                string cidade = dados.cidade;

                string endereco = rua + ", " + bairro ;
                txtCidade.Text = cidade;
                txtEndereco.Text = endereco;

                cboUF.SelectedItem = dados.uf;
            }
            catch (Exception)
            {

                MessageBox.Show("CEP inválido, digite novamente!",
                                "SIGMA",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

        }

        private void lblNome_Click(object sender, EventArgs e)
        {

        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblDepartamento_Click(object sender, EventArgs e)
        {

        }

        private void cboDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void imgFoto_Click(object sender, EventArgs e)
        {

        }

        private void lblEndereco_Click(object sender, EventArgs e)
        {

        }

        private void lblCEP_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
