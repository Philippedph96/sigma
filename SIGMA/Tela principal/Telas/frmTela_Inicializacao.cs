﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;


namespace Tela_principal
{
    public partial class frmTela_Inicializacao : Form
    {
        public frmTela_Inicializacao()
        {
            InitializeComponent();
            //UserSession.UsuarioLogado.Acesso_Menu = false;

        }

        
        private void timerpb_Tick(object sender, EventArgs e)
        {
            //Modo de fazer a progess bar só carregar(Style -Blocks e MarqeeAnimation... 100)

            if (progressBar1.Value != 100)
            {
                progressBar1.Value++;
            }
            else
            {
                //para o timer
                timerpb.Stop();
                LoginUsuario tela = new LoginUsuario();
                tela.Show();


                //Fecha essa tela
                this.Hide();

            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //Anotações Gambiarradas
            timerpb.Enabled = true;
            //Inicia o tempo
            timerpb.Start();
            //Intervalo em milisegundos
            timerpb.Interval = 200;
            //Quanto aumenta na progess bar
            progressBar1.Maximum = 10;

            //Refaz o evento 
            timerpb.Tick += new EventHandler(timerpb_Tick);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
