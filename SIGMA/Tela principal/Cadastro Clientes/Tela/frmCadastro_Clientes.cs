﻿using Correios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_de_Clientes;

namespace Tela_principal.Telas
{
    public partial class frmCadastro_Clientes : Form
    {
        DTO_Cliente dto = new DTO_Cliente();
        public bool Ativar(bool ativar)
        {
            if (rdnFisica.Checked == true)
            {
                txtCNPJ.Enabled = false;
                txtCPF.Enabled = true;
                txtRG.Enabled = true;
                chkF.Enabled = true;
                chkM.Enabled = true;

                return true;
            }
            else
            {
                txtRG.Enabled = false;
                txtCPF.Enabled = false;
                chkF.Enabled = false;
                chkM.Enabled = false;
                txtCNPJ.Enabled = true;

                return false;
            }
        }
        public frmCadastro_Clientes()
        {
            InitializeComponent();

            List<string> carta = new List<string>();
            carta.Add("A");
            carta.Add("B");
            carta.Add("C");
            carta.Add("D");
            carta.Add("E");
            carta.Add("E");
            carta.Add("ACC");
            carta.Add("MOTOR-CASA");

            cboCarta.DataSource = carta;

            List<string> uf = new List<string>();
            uf.Add("AC");
            uf.Add("AL");
            uf.Add("AM");
            uf.Add("AP");
            uf.Add("BA");
            uf.Add("CE");
            uf.Add("DF");
            uf.Add("ES");
            uf.Add("GO");
            uf.Add("MA");
            uf.Add("MG");
            uf.Add("MS");
            uf.Add("MT");
            uf.Add("PA");
            uf.Add("PB");
            uf.Add("PE");
            uf.Add("PI");
            uf.Add("PR");
            uf.Add("RJ");
            uf.Add("RN");
            uf.Add("RO");
            uf.Add("RR");
            uf.Add("RS");
            uf.Add("SC");
            uf.Add("SE");
            uf.Add("SP");
            uf.Add("TO");

            cboUF.DataSource = uf;
            Ativar(rdnFisica.Checked = true);
        }
        public bool VerificarRG (string rg)
        {
            
            // Multiplicar os numeros de acordo com a sequencia de 2 a 9
            // Somar os valores que foram multiplicados
            // Pegar o RESTO da SOMA por 11, para assim, verificar se o RG é válido.
            try
            {
                rg = txtRG.Text.Replace(".", "").Replace("-", "").Replace(",", "").Replace("/", "").Replace(" ", "");
                // Pegando os 8 primeiros digitos digitalizados pelo usuário
                int n1 = int.Parse(rg.Substring(0, 1));
                int n2 = int.Parse(rg.Substring(1, 1));
                int n3 = int.Parse(rg.Substring(2, 1));
                int n4 = int.Parse(rg.Substring(3, 1));
                int n5 = int.Parse(rg.Substring(4, 1));
                int n6 = int.Parse(rg.Substring(5, 1));
                int n7 = int.Parse(rg.Substring(6, 1));
                int n8 = int.Parse(rg.Substring(7, 1));

                // Para verificar o digito verificador
                string dv = rg.Substring(8, 1);

                //Primeiro multiplique os valores acima em uma sequência de 2 à 9
                //Depois some esse valor.
                int soma = n1 * 2 + n2 * 3 + n3 * 4 + n4 * 5 + n5 * 6 + n6 * 7 + n7 * 8 + n8 * 9;

                //Pegue o valor total daquela soma e divida por 11 pegando apenas o RESTO da divisão
                string digitoverificador = Convert.ToString(soma % 11);

                //Se o digito Verificador for igual a 1, automagicamente o mesmo se tornará o algarismo Romano X...
                //Pois será efetuado o cálculo, no caso, 11 - 1(Digito verificador), dando o valor de 10. 

                if (digitoverificador == "1")
                {
                    digitoverificador = "X";
                }

                //Se o digito verificador for igual a 0, automagicamente o mesmo se tornará 0.
                //Pois 11 - 0 (Digito verificador) é igual a 11, sendo que o digito 11 não é permitido
                else if (digitoverificador == "0") 
                {
                    digitoverificador = "0";
                }
                // Se não for nenhuma das condições feita acima, ele irá fazer o cálculo 11 - Digito Verificador
                else
                {
                    digitoverificador = (11 - int.Parse(digitoverificador)).ToString();
                }

                // Verificar se o dígito é igual os digitado pelo usuário
                if (dv == digitoverificador)
                {
                    return true;
                }
                else
                {
                    return false;   
                }

            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool VerificarCPF (string cpf)
        {
            try
            {
                if (!(txtCPF.Text.Length == 14))
                {
                    cpf = txtCPF.Text.Replace (".", "").Replace("-","").Replace(",","").Replace("/","").Replace(" ","");

                    int n1 = int.Parse(cpf.Substring(0, 1));
                    int n2 = int.Parse(cpf.Substring(1, 1));
                    int n3 = int.Parse(cpf.Substring(2, 1));
                    int n4 = int.Parse(cpf.Substring(3, 1));
                    int n5 = int.Parse(cpf.Substring(4, 1));
                    int n6 = int.Parse(cpf.Substring(5, 1));
                    int n7 = int.Parse(cpf.Substring(6, 1));
                    int n8 = int.Parse(cpf.Substring(7, 1));
                    int n9 = int.Parse(cpf.Substring(8, 1));

                    int digito1 = int.Parse(cpf.Substring(9, 1));
                    int digito2 = int.Parse(cpf.Substring(10, 1));

                    //Está verificando se os números são iguais. Se forem, o número colocado é burlado
                    if (n1 == n2 && n2 == n3 && n3 == n4 && n4 == n5 && n5 == n6 && n6 == n7 && n7 == n8 && n8 == n9)
                    {
                        return false;
                    }

                    // Aqui está sendo multiplicado e somado todos os números que o usuário digitou em uma sequência de 10 à 2.
                    int soma1 = n1 * 10 + n2 * 9 + n3 * 8 + n4 * 7 + n5 * 6 + n6 * 5 + n7 * 4 + n8 * 3 + n9 * 2;

                    // Logo após, é pego o total da soma e é dividido por 11 pegando o RESTO dessa divisão. 
                    int digitoverificador1 = soma1 % 11;

                    // Verificar se o valor é menor ou maior que 2
                    if (digitoverificador1 < 2)
                    {
                        digitoverificador1 = 0;
                    }
                    else
                    {
                        digitoverificador1 = 11 - digitoverificador1;
                    }

                    // Agora, basicamente, é feito o mesmo processo...
                    // Soma e multiplicar todos os números em uma sequência de agora 11 números
                    int soma2 = n1 * 11 + n2 * 10 + n3 * 9 + n4 * 8 + n5 * 7 + n6 * 6 + n7 * 5 + n8 * 4 + n9 * 3 + digitoverificador1 * 2;

                    // Logo após, é pego o total da soma e é dividido por 11 pegando o RESTO dessa divisão. 
                    int digitoverificador2 = soma2 % 11;

                    // Verificar se o valor é menor ou maior que 2
                    if (digitoverificador1 < 2)
                    {
                        digitoverificador1 = 0;
                    }
                    else
                    {
                        digitoverificador1 = 11 - digitoverificador1;
                    }

                    // Verificar se os dois dígitos são iguais as digitados pelo usuário
                    if (digito1 == digitoverificador1 && digito2 == digitoverificador2)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }
        public bool VerificarCNPJ (string cnpj)
        {
            // Primeiro deve-se colocar os 12 primeiros números em variáveis do tipo INTEIRO
            // Segundo Calcular esses 12 números e "gerar" os 2 digitos verificadores
            // E por fim, verificar se o CNPJ é verdadeiro ou falso.

            try
            {
                    cnpj = txtCNPJ.Text.Replace(".", "").Replace("-", "").Replace(",", "").Replace("/", "").Replace(" ", "");
                    int n1 = int.Parse(cnpj.Substring(0, 1));
                    int n2 = int.Parse(cnpj.Substring(1, 1));
                    int n3 = int.Parse(cnpj.Substring(2, 1));
                    int n4 = int.Parse(cnpj.Substring(3, 1));
                    int n5 = int.Parse(cnpj.Substring(4, 1));
                    int n6 = int.Parse(cnpj.Substring(5, 1));
                    int n7 = int.Parse(cnpj.Substring(6, 1));
                    int n8 = int.Parse(cnpj.Substring(7, 1));
                    int n9 = int.Parse(cnpj.Substring(8, 1));
                    int n10 = int.Parse(cnpj.Substring(9, 1));
                    int n11 = int.Parse(cnpj.Substring(10, 1));
                    int n12 = int.Parse(cnpj.Substring(11, 1));

                    int digito1 = int.Parse(cnpj.Substring(12, 1));
                    int digito2 = int.Parse(cnpj.Substring(13, 1));

                    if (n1 == 0 && n2 == 0 && n3 == 0 && n4 == 0 && n5 == 0 && n6 == 0 && n7 == 0 && n8 == 0 && n9 == 0 && n10 == 0 && n11 == 0 && n12 == 0)
                    {
                        return false;
                    }

                    int soma1 = n1 * 5 + n2 * 4 + n3 * 3 + n4 * 2 + n5 * 9 + n6 * 8 + n7 * 7 + n8 * 6 + n9 * 5 + n10 * 4 + n11 * 3 + n12 * 2;
                    int digitoverificador1 = soma1 % 11;

                    if (digitoverificador1 < 2)
                    {
                        digitoverificador1 = 0;
                    }
                    else
                    {
                        digitoverificador1 = 11 - digitoverificador1;
                    }

                    int soma2 = n1 * 6 + n2 * 5 + n3 * 4 + n4 * 3 + n5 * 2 + n6 * 9 + n7 * 8 + n8 * 7 + n9 * 6 + n10 * 5 + n11 * 4 + n12 * 3 + digitoverificador1 * 2;
                    int digitoverificador2 = soma2 % 11;

                    if (digitoverificador2 < 2)
                    {
                        digitoverificador2 = 0;
                    }
                    else
                    {
                        digitoverificador2 = 11 - digitoverificador2;
                    }

                    if (digito1 == digitoverificador1 && digito2 == digitoverificador2)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
            }
            catch (Exception)
            {

                return false;
            }
        }
        public void Automagicamente ()
        {
            //try
            //{
                DTO_Cliente dto = new DTO_Cliente();
                lblNumero.Text = dto.ID.ToString();
                dto.CPF = txtCPF.Text;
                dto.CNPJ = txtCNPJ.Text;
                dto.RG = txtRG.Text;

           
            if (VerificarCNPJ(dto.CNPJ) == false)
            {
                if (txtCNPJ.Text != string.Empty || txtCNPJ.Text != null || txtCNPJ.Enabled == true)
                {
                    if (VerificarRG(dto.RG) == false)
                    {
                        MessageBox.Show("RG inválido!" + "\n" + "Verifique se o RG está digitalizado da maneira correta",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                        return;
                    }

                    dto.Nome = txtNome.Text;
                    dto.Profissao = txtProfissao.Text;
                    dto.Tipo_Carta = cboCarta.SelectedItem.ToString();
                    dto.DatadeNascimento = Convert.ToDateTime(dtpNascimento.Value);
                    dto.Endereco = txtEndereco.Text;
                    dto.CEP = txtCEP.Text;
                    dto.UF = cboUF.SelectedItem.ToString();
                    dto.Complemento = txtComplemento.Text;
                    dto.Numero = int.Parse(txtNumero.Text);
                    dto.Email = txtEmail.Text;
                    dto.Telefone = txtTelefone.Text;
                    dto.Celular = txtCelular.Text;
                    dto.Telefone_Residencial = txtTelefoneResidencial.Text;
                    dto.ClienteDesde = DateTime.Now;

                    if (chkF.Checked == true)
                    {
                        dto.Sexo = true;
                    }
                    else if (chkM.Checked == true)
                    {
                        dto.Sexo = false;
                    }

                    Business_Cliente data = new Business_Cliente();
                    data.Salvar(dto);

                    MessageBox.Show("Cliente cadastrado com sucesso",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    MessageBox.Show("CNPJ inválido!" + "\n" + "Verifique se o CNPJ está digitalizado da maneira correta",
                                   "Sigma",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Error);
                    return;
                }
            }
                
                if (VerificarCPF(dto.CPF) == false)
                {
                    if (true)
                    {
                    if (VerificarRG(dto.RG) == false)
                    {
                        MessageBox.Show("RG inválido!" + "\n" + "Verifique se o RG está digitalizado da maneira correta",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                        return;
                    }

                    dto.Nome = txtNome.Text;
                    dto.Profissao = txtProfissao.Text;
                    dto.Tipo_Carta = cboCarta.SelectedItem.ToString();
                    dto.DatadeNascimento = Convert.ToDateTime(dtpNascimento.Value);
                    dto.Endereco = txtEndereco.Text;
                    dto.CEP = txtCEP.Text;
                    dto.UF = cboUF.SelectedItem.ToString();
                    dto.Complemento = txtComplemento.Text;
                    dto.Numero = int.Parse(txtNumero.Text);
                    dto.Email = txtEmail.Text;
                    dto.Telefone = txtTelefone.Text;
                    dto.Celular = txtCelular.Text;
                    dto.Telefone_Residencial = txtTelefoneResidencial.Text;
                    dto.ClienteDesde = DateTime.Now;

                    if (chkF.Checked == true)
                    {
                        dto.Sexo = true;
                    }
                    else if (chkM.Checked == true)
                    {
                        dto.Sexo = false;
                    }

                    Business_Cliente banco = new Business_Cliente();
                    banco.Salvar(dto);

                    MessageBox.Show("Cliente cadastrado com sucesso",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                    
                }
                    else
                {

                }
                    MessageBox.Show("CPF inválido!" + "\n" + "Verifique se o CPF está digitalizado da maneira correta",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                    return;
                }

                if (VerificarRG(dto.RG) == false)
                {
                    MessageBox.Show("RG inválido!" + "\n" + "Verifique se o RG está digitalizado da maneira correta",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                    return;
                }

                dto.Nome = txtNome.Text;
                dto.Profissao = txtProfissao.Text;
                dto.Tipo_Carta = cboCarta.SelectedItem.ToString();
                dto.DatadeNascimento = Convert.ToDateTime(dtpNascimento.Value);
                dto.Endereco = txtEndereco.Text;
                dto.CEP = txtCEP.Text;
                dto.UF = cboUF.SelectedItem.ToString();
                dto.Complemento = txtComplemento.Text;
                dto.Numero = int.Parse(txtNumero.Text);
                dto.Email = txtEmail.Text;
                dto.Telefone = txtTelefone.Text;
                dto.Celular = txtCelular.Text;
                dto.Telefone_Residencial = txtTelefoneResidencial.Text;
                dto.ClienteDesde = DateTime.Now;

                if (chkF.Checked == true)
                {
                    dto.Sexo = true;
                }
                else if (chkM.Checked == true)
                {
                    dto.Sexo = false;
                }

                Business_Cliente db = new Business_Cliente();
                db.Salvar(dto);

                MessageBox.Show("Cliente cadastrado com sucesso",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            //}
            //catch (ArgumentException ex)
            //{
            //    MessageBox.Show("ERRO!" + "\n" + ex.Message,
            //                     "Sigma",
            //                     MessageBoxButtons.OK,
            //                     MessageBoxIcon.Error);
            //}
            //catch (Exception erro)
            //{
            //    MessageBox.Show("ERRO!" + "\n" + erro.Message,
            //                     "Sigma",
            //                     MessageBoxButtons.OK,
            //                     MessageBoxIcon.Error);
            //}
        }
        public void Desaparecer()
        {
            try
            {

            DialogResult dialog = MessageBox.Show("Deseja realmente fechar este programa?",
                                                        "Projeto Sigma",
                                                        MessageBoxButtons.YesNo,
                                                        MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                    frmTela_Principal tela = new frmTela_Principal();
                    tela.Show();
                    this.Close();
            }

            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }
        public void Abracadabra ()
        {
            try
            {

                DTO_Cliente dto = new DTO_Cliente();
                lblNumero.Text = dto.ID.ToString();
                if (rdnJuridica.Checked == true)
                {
                    lblCNPJ.Text = "CNPJ";
                    dto.CPF = txtCNPJ.Text;
                }
                else
                {
                    dto.CPF = txtCNPJ.Text;
                }
                dto.Nome = txtNome.Text;

                VerificarRG(dto.RG);

                dto.RG = txtRG.Text;
                dto.Profissao = txtProfissao.Text;
                dto.Tipo_Carta = cboCarta.SelectedItem.ToString();
                dto.DatadeNascimento = Convert.ToDateTime(dtpNascimento.Value);
                dto.Endereco = txtEndereco.Text;
                dto.CEP = txtCEP.Text;
                dto.UF = cboUF.SelectedItem.ToString();
                dto.Complemento = txtComplemento.Text;
                dto.Numero = int.Parse(txtNumero.Text);
                dto.Email = txtEmail.Text;
                dto.Telefone = txtTelefone.Text;
                dto.Celular = txtCelular.Text;
                dto.Telefone_Residencial = txtTelefoneResidencial.Text;
                dto.ClienteDesde = DateTime.Now;
                if (chkF.Checked == true)
                {
                    dto.Sexo = chkF.Checked;
                }
                else if (chkM.Checked == true)
                {
                    dto.Sexo = chkM.Checked;
                }


                Business_Cliente db = new Business_Cliente();
                db.Alterar(dto);

                MessageBox.Show("Cliente cadastrado com sucesso",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        public void Alacazum()
        {
            try
            {
                           
            DialogResult dialog =  MessageBox.Show("Você realmente deseja deletar esse registro?",
                            "Sigma",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                DTO_Cliente dto = new DTO_Cliente();
                int id = dto.ID;

                Business_Cliente db = new Business_Cliente();
                db.Remover(id);
            }
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                  "Sigma",
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Error);
            }
        }
        public void Magicamente()
        {
            DialogResult dialog = MessageBox.Show("Você tem certeza que deseja cancelar o cadastro?",
                                                  "Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                this.Hide();

                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
            }
        }
        
        private void lblFechar_Click(object sender, EventArgs e)
        {
            Desaparecer();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Automagicamente();
        }
        private void rdnFisica_Click(object sender, EventArgs e)
        {
            if (Ativar(rdnFisica.Checked) == true)
            {
                return;
            }
        }

        private void rdnJuridica_Click(object sender, EventArgs e)
        {
            if (Ativar(rdnJuridica.Checked) == true)
            {
                return;
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {

            Abracadabra();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Alacazum();
        }
        
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Magicamente();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Desaparecer();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
            this.Close();
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Esse campo aceita apenas Números",
                                "SIGMA",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CorreiosApi service = new CorreiosApi();
                var cep = txtCEP.Text;
                var dados = service.consultaCEP(cep);

                string rua = dados.end;
                string bairro = dados.bairro;
                string cidade = dados.cidade;

                string endereco = rua + ", " + bairro + ", " + cidade;

                txtEndereco.Text = endereco;

                cboUF.SelectedItem = dados.uf;
            }
            catch (Exception)
            {

                MessageBox.Show("CEP invalido , digite novamente! \n",
                                "SIGMA",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
           
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Automagicamente();
            }
        }

    }
}
