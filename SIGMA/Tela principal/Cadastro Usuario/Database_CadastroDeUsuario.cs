﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_Usuario
{
    class Database_CadastroDeUsuario
    {
        public int Salvar(DTO_CadastroDeUsuarios dto) {
            string script =
            @"INSERT INTO tb_usuario
            ( nm_nome, nm_usuario, nr_senha,ds_status,ds_observacao)
            VALUES
            (@nm_nome,@nm_usuario,@nr_senha,@ds_status,@ds_observacao)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("nr_senha", dto.Senha));
            parms.Add(new MySqlParameter("ds_status", dto.Status));
            parms.Add(new MySqlParameter("ds_observacao", dto.Obeservacao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
            }

        public int Alterar(DTO_CadastroDeUsuarios dto) {
            string script =
               @"Update tb_usuario SET
            nm_nome = @nm_nome,nm_usuario = @nm_usuario,nr_senha = @nr_senha, ds_status = @ds_status,ds_observacao = @ds_observacao
            Where id_usuario";


            //alterar diferente do salvar ele prescisa do ID

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_usuario", dto.ID));
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("nr_senha", dto.Senha));
            parms.Add(new MySqlParameter("ds_status", dto.Status));
            parms.Add(new MySqlParameter("ds_observacao", dto.Obeservacao));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int id)
        {
            string script = @"DELETE  from tb_usuario Where id_usuario = @id_usuario";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_usuario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }

    }
}
