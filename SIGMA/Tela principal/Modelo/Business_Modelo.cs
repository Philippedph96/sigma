﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.LModelodoVeiculo
{
    class Business_Modelo
    {
        public List<DTO_Modelo> Consultar(DTO_Modelo dto)
        {
            if (dto.Modelo == string.Empty)
            {
                throw new ArgumentException("O Modelo de busca precisa ser preenchido!");
            }

            Database_Modelo db = new Database_Modelo();
            List<DTO_Modelo> list = db.Consultar(dto);

            return list;
        }

        public List<DTO_Modelo> Listar()
        {
            Database_Modelo db = new Database_Modelo();
            List<DTO_Modelo> list = db.Listar();

            return list;
        }
    }
}
