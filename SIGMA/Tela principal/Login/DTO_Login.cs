﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Login
{
    class DTO_Login
    {

        public int ID  { get; set; } //OK
        public string Nome { get; set; }
        public string Usuario { get; set; } //ok
        public string Senha { get; set; } //ok
        public string Status { get; set; } //Ok
        public string Observacao { get; set; }
    }
}
