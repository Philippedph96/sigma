﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Cadastro_de_Clientes
{
    class DataBase_CadastrodeCliente
    {
        public int Salvar(DTO_CadastroDeCliente dto)
        {
            string script =
            @"INSERT INTO tb_Cliente
           (
               nm_nome,
               ds_cpf,
               ds_profissao,
               dt_nascimento,
               dt_cliente_desde,
               ds_sexo,
               ds_endereco,
               nr_CEP,
               ds_complemento,
               nr_numero,
               ds_email,
               nr_telefone_residencial,
               nr_telefone_celular,
               nr_telefone_comercial
               ds_tipodecarta
               ds_rg
           )
           VALUES
           (
               @nm_nome,
               @ds_cpf,
               @ds_profissao,
               @dt_nascimento,
               @dt_cliente_desde,
               @ds_sexo,
               @ds_endereco,
               @nr_CEP,
               @ds_complemento,
               @nr_numero,
               @ds_email,
               @nr_telefone_residencial,
               @nr_telefone_celular,
               @nr_telefone_comercial
               @ds_tipodecarta
               @ds_rg
           )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_nome", dto.Nome));
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parm.Add(new MySqlParameter("ds_profissao", dto.Profissao));
            parm.Add(new MySqlParameter("dt_nascimento", dto.DatadeNascimento));
            parm.Add(new MySqlParameter("dt_cliente_desde", dto.ClienteDesde));
            parm.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parm.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parm.Add(new MySqlParameter("nr_cep", dto.CEP));
            parm.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parm.Add(new MySqlParameter("nr_numero", dto.Numero));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("nr_telefone_residencial", dto.Telefone_residencial));
            parm.Add(new MySqlParameter("nr_telefone_celular", dto.Telefone_celular));
            parm.Add(new MySqlParameter("nr_telefone_comercial", dto.Telefone_comercial));
            parm.Add(new MySqlParameter("ds_tipodecarta", dto.Tipo_da_carta));
            parm.Add(new MySqlParameter("ds_rg", dto.RG));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public void Alterar(DTO_CadastroDeCliente dto)
        {
            string script =
                @"UPDATE tb_Cliente
                    SET nm_nome                     = @nm_nome,
                        ds_cpf                      = @ds_cpf,
                        ds_profissao                = @ds_profissao,
                        dt_nascimento               = @dt_nascimento,
                        dt_cliente_desde            = @dt_cliente_desde,
                        ds_sexo                     = @ds_sexo,
                        ds_endereco                 = @ds_endereco,
                        nr_CEP                      = @nr_CEP,
                        ds_complemento              = @ds_complemento,
                        nr_numero                   = @nr_numero,
                        ds_email                    = @ds_email,
                        nr_telefone_residencial     = @nr_telefone_residencial,
                        nr_telefone_celular         = @nr_telefone_celular,
                        nr_telefone_comercial       = @nr_telefone_comercial,
                        ds_tipodecarta              = @ds_tipodecarta, 
                        ds_rg                       = @ds_rg                                     
                   WHERE id_cliente                 = @id_cliente";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_cliente", dto.ID));
            parm.Add(new MySqlParameter("nm_nome", dto.Nome));
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parm.Add(new MySqlParameter("ds_profissao", dto.Profissao));
            parm.Add(new MySqlParameter("dt_nascimento", dto.DatadeNascimento));
            parm.Add(new MySqlParameter("dt_cliente_desde", dto.ClienteDesde));
            parm.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parm.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parm.Add(new MySqlParameter("nr_cep", dto.CEP));
            parm.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parm.Add(new MySqlParameter("nr_numero", dto.Numero));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("nr_telefone_residencial", dto.Telefone_residencial));
            parm.Add(new MySqlParameter("nr_telefone_celular", dto.Telefone_celular));
            parm.Add(new MySqlParameter("nr_telefone_comercial", dto.Telefone_comercial));
            parm.Add(new MySqlParameter("ds_tipodecarta", dto.Tipo_da_carta));
            parm.Add(new MySqlParameter("ds_rg", dto.RG));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }

        public void Remover(int dto)
        {
            string script =
                @"DELETE FROM tb_cliente
                       WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_cliente", dto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }


    }
}
