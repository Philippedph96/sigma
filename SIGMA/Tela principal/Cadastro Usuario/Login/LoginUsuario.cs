﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_Usuario;
using Tela_principal.Login;

namespace Tela_principal
{
    public partial class LoginUsuario : Form
    {
        public LoginUsuario()
        {
            //Thread t = new Thread(new ThreadStart(StartForm));
            //t.Start();
            //Thread.Sleep(50);
            InitializeComponent();
            //t.Abort();


        }

        //public void StartForm()
        //{
        //    Application.Run(new frmTela_Inicializacao());

        //}

        public void Logar()
        {
            try
            {
                DTO_Usuarios dto = new DTO_Usuarios();
                
                dto.Usuario = txtUsuario.Text;
                string senha = txtSenha.Text;
                
                SHA256Cript crip = new SHA256Cript();
                dto.Senha = crip.Criptografar(senha);
                
                Business_Usuario db = new Business_Usuario();
                bool logou = db.Logar(dto);

            if (logou == true)
            {
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
                MessageBox.Show("Seja bem vindo!!",
                            "Sigma",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    // não seria .Close();
                    this.Hide();
                    // Deveria sim, mas com o .Close esta fechando o programa, e com o .Hide esta funcionando tranquilamente!

            }
            else
            {
                DialogResult re = MessageBox.Show("Credênciais inválidas!!" + "\n" + "Deseja realizar Cadastro?",
                                 "Sigma",
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Error);
                if (re == DialogResult.Yes)
                {
                    frmCadastro_Usuario tela = new frmCadastro_Usuario();
                    tela.ShowDialog();
                    this.Close();
                }
            }

            }
            catch (ArgumentException erro)
            {

                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
            catch(Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }


        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar este programa?",
                                                  "Projeto Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja cancelar o login?",
                                                 "Projeto Sigma",
                                                 MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Logar();
        }

        private void label23_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar está tela?",
                                                  "Projeto Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Logar();
            }
        }
    }
}
