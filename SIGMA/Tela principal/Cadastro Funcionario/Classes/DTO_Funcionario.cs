﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_funcionario
{
    class DTO_Funcionario
    {
        public int ID { get; set; } //ok
        public int ID_Departamento { get; set; } //ok
        public string Nome { get; set; } //ok
        public DateTime DatadeNascimento { get; set; } //ok
        public string CPF { get; set; } //ok
        public string Endereco { get; set; } //ok
        public string Complemento { get; set; } //ok
        public bool Sexo { get; set; } //ok
        public string Foto { get; set; } // ok
        public string CEP { get; set; } //ok
        public string UF { get; set; }
        public int Numero { get; set; } //ok
        public string Email { get; set; } //ok
        public string Telefone { get; set; } //ok
        public string Celular { get; set; } //ok
        public string TelefoneResidencial { get; set; } //ok
        public string Cidade { get; set; } //ok

        //Foto tem que ser alterada 

        //UF,departamento outra tabela 
    }
}
