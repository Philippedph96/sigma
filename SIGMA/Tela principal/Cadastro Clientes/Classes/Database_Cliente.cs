﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Cadastro_de_Clientes
{
    class Database_Cliente
    {
        public int Salvar(DTO_Cliente dto)
        {
            string script =
            @"INSERT INTO tb_cliente
           (
               ds_cpf,
               ds_cnpj,
               ds_rg,
               nm_nome,
               dt_data_nascimento,
               ds_cep,
               ds_uf,
               ds_endereco,
               ds_telefone_residencial,
               ds_celular,
               ds_telefone,
               ds_profissao,
               ds_tipo_de_carta,
               dt_cliente_desde,
               ds_email,
               ds_sexo,
               ds_complemento,
               nr_numero
           )
           VALUES
           (
               @ds_cpf,
               @ds_cnpj,
               @ds_rg,
               @nm_nome,
               @dt_data_nascimento,
               @ds_cep,
               @ds_uf,
               @ds_endereco,
               @ds_telefone_residencial,
               @ds_celular,
               @ds_telefone,
               @ds_profissao,
               @ds_tipo_de_carta,
               @dt_cliente_desde,
               @ds_email,
               @ds_sexo,
               @ds_complemento,
               @nr_numero
           )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parm.Add(new MySqlParameter("ds_cnpj", dto.CNPJ));
            parm.Add(new MySqlParameter("ds_rg", dto.RG));
            parm.Add(new MySqlParameter("nm_nome", dto.Nome));
            parm.Add(new MySqlParameter("dt_data_nascimento", dto.DatadeNascimento));
            parm.Add(new MySqlParameter("ds_cep", dto.CEP));
            parm.Add(new MySqlParameter("ds_uf", dto.UF));
            parm.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parm.Add(new MySqlParameter("ds_telefone_residencial", dto.Telefone_Residencial));
            parm.Add(new MySqlParameter("ds_celular", dto.Celular));
            parm.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parm.Add(new MySqlParameter("ds_profissao", dto.Profissao));
            parm.Add(new MySqlParameter("ds_tipo_de_carta", dto.Tipo_Carta));
            parm.Add(new MySqlParameter("dt_cliente_desde", dto.ClienteDesde));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parm.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parm.Add(new MySqlParameter("nr_numero", dto.Numero));
            

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public void Alterar(DTO_Cliente dto)
        {
            string script =
                @"UPDATE tb_Cliente
                    SET             ds_cpf                      = @ds_cpf,
                                    ds_cnpj                     = @ds_cnpj,
                                    ds_rg                       = @ds_rg,
                                    nm_nome                     = @nm_nome,
                                    dt_data_nascimento          = @dt_data_nascimento,
                                    ds_cep                      = @ds_cep,
                                    ds_uf                       = @ds_uf,
                                    ds_endereco                 = @ds_endereco,
                                    ds_telefone_residencial     = @ds_telefone_residencial,
                                    ds_celular                  = @ds_celular,
                                    ds_telefone                 = @ds_telefone,
                                    ds_profissao                = @ds_profissao,
                                    ds_tipo_de_carta            = @ds_tipo_de_carta,
                                    dt_cliente_desde            = @dt_cliente_desde,
                                    ds_email                    = @ds_email,
                                    ds_sexo                     = @ds_sexo,
                                    ds_complemento              = @ds_complemento,
                                    nr_numero                   = @nr_numero                    
                               WHERE id_cliente                 = @id_cliente";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_cliente", dto.ID));
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parm.Add(new MySqlParameter("ds_cnpj", dto.CNPJ));
            parm.Add(new MySqlParameter("ds_rg", dto.RG));
            parm.Add(new MySqlParameter("nm_nome", dto.Nome));
            parm.Add(new MySqlParameter("dt_data_nascimento", dto.DatadeNascimento));
            parm.Add(new MySqlParameter("ds_cep", dto.CEP));
            parm.Add(new MySqlParameter("ds_uf", dto.UF));
            parm.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parm.Add(new MySqlParameter("nr_telefone_residencial", dto.Telefone_Residencial));
            parm.Add(new MySqlParameter("nr_telefone_celular", dto.Celular));
            parm.Add(new MySqlParameter("nr_telefone_comercial", dto.Telefone));
            parm.Add(new MySqlParameter("ds_profissao", dto.Profissao));
            parm.Add(new MySqlParameter("dt_cliente_desde", dto.Tipo_Carta));
            parm.Add(new MySqlParameter("dt_cliente_desde", dto.ClienteDesde));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parm.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parm.Add(new MySqlParameter("nr_numero", dto.Numero));
            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }

        public void Remover(int dto)
        {
            string script =
                @"DELETE FROM tb_cliente
                       WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_cliente", dto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }

        public List<DTO_Cliente> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Cliente> fora = new List<DTO_Cliente>();

            while (reader.Read())
            {
                DTO_Cliente dentro = new DTO_Cliente();
                dentro.ID = reader.GetInt32("id_cliente");
                dentro.CPF = reader.GetString("ds_cpf");
                dentro.CNPJ = reader.GetString("ds_cnpj");
                dentro.RG = reader.GetString("ds_rg");
                dentro.Nome = reader.GetString("nm_nome");
                dentro.DatadeNascimento = reader.GetDateTime("dt_data_nascimento");
                dentro.CEP = reader.GetString("ds_cep");
                dentro.UF = reader.GetString("ds_uf");
                dentro.Endereco = reader.GetString("ds_endereco");
                dentro.Telefone_Residencial = reader.GetString("ds_telefone_residencial");
                dentro.Telefone = reader.GetString("ds_telefone");
                dentro.Celular = reader.GetString("ds_celular");
                dentro.Profissao = reader.GetString("ds_profissao");
                dentro.Tipo_Carta = reader.GetString("ds_tipo_de_carta");
                dentro.ClienteDesde = reader.GetDateTime("dt_cliente_desde");
                dentro.Email = reader.GetString("ds_email");
                dentro.Sexo = reader.GetBoolean("ds_sexo");
                dentro.Complemento = reader.GetString("ds_complemento");
                dentro.Numero = reader.GetInt32("nr_numero");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }


    }
}
