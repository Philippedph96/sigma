﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_de_funcionario;
using Tela_principal.Fim_Orcamento;
using Tela_principal.LModelodoVeiculo;

namespace Tela_principal.Orçamento
{
    public partial class frmFim_Orcamento : Form
    {
        public frmFim_Orcamento()
        {
            InitializeComponent();
            CarregarGrid();
            CarregarCombos();
        }


        public void CarregarCombos()
        {
            Business_Funcionario db = new Business_Funcionario();
            List<DTO_Funcionario> lista = db.Listar();

            cboFuncionarios.ValueMember = nameof(DTO_Funcionario.ID);
            cboFuncionarios.DisplayMember = nameof(DTO_Funcionario.Nome);

            cboFuncionarios.DataSource = lista;
        }
        void CarregarGrid()
        {

            Database_Orcamento db = new Database_Orcamento();
            List<VIEW_Fim_orcamento> list = db.Consultar();

            dgvinfo.AutoGenerateColumns = false;
            dgvinfo.DataSource = list;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            this.Close();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar este programa?",
                                                 "Projeto Sigma",
                                                 MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
