﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_Clientes
{
    class Business_CadastroDeCliente
    {
        public  int Salvar(DTO_CadastroDeCliente dto) {
            if (dto.Nome == string.Empty)
            {
                System.Windows.Forms.MessageBox.Show("Nome é Obrigatorio ");
            }
            if (dto.Profissao == string.Empty)
            {
                System.Windows.Forms.MessageBox.Show("Profissão é Obrigatorio");
            }
            DataBase_CadastrodeCliente db = new DataBase_CadastrodeCliente();
            return db.Salvar(dto);
        }
        
    }
}
