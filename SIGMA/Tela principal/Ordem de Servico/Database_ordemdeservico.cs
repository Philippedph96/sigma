﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Ordem_de_Servico
{
    class Database_ordemdeservico
    {
        //Salvar 
        public int Salvar(DTO_ordemdeservico dto)
        {
            string script = 
            @"INSERT INTO TB_Ordem_Servico
            (
                dt_data_entrega,
                dt_data_saida,
                ds_observacoes
            )
            VALUES
            (
                @dt_data_entrega,
                @dt_data_saida,
                @ds_observacoes
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("dt_data_entrega", dto.DataDeEntrega));
            parm.Add(new MySqlParameter("dt_data_saidaa", dto.DataDesaida));
            parm.Add(new MySqlParameter("dt_observacoes", dto.Observacoes));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);

        }

        //Alterar

        public void Alterar(DTO_ordemdeservico dto)
        {
            string script =
                @"UPDATE tb_Cliente
                     SET dt_data_entrega        = @dt_data_entrega,
                         dt_data_saida          = @dt_data_saida,
                         ds_observacoes         = @ds_observacoes
                  WHERE  id_ordem_servico       = @id_ordem_servico";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_ordem_servico", dto.ID));
            parm.Add(new MySqlParameter("dt_data_entrega  ", dto.DataDeEntrega));
            parm.Add(new MySqlParameter("dt_data_saida", dto.DataDesaida));
            parm.Add(new MySqlParameter("dt_observacoes", dto.Observacoes));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);

        }
    }
}
