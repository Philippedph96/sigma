﻿namespace Tela_principal.Cadastro_Veiculos
{
    partial class frm_consu_alte_etc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_consu_alte_etc));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cnpj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.excluir = new System.Windows.Forms.DataGridViewImageColumn();
            this.alterar = new System.Windows.Forms.DataGridViewImageColumn();
            this.button7 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lblFechar = new System.Windows.Forms.Label();
            this.cboProprietário = new System.Windows.Forms.ComboBox();
            this.txtPlaca = new System.Windows.Forms.MaskedTextBox();
            this.cboModelo = new System.Windows.Forms.ComboBox();
            this.cboAnoFabricacao = new System.Windows.Forms.ComboBox();
            this.lblAnoFabricacao = new System.Windows.Forms.Label();
            this.txtCor = new System.Windows.Forms.TextBox();
            this.lblCor = new System.Windows.Forms.Label();
            this.txtOdometro = new System.Windows.Forms.TextBox();
            this.lblOdometro = new System.Windows.Forms.Label();
            this.cboCombustivel = new System.Windows.Forms.ComboBox();
            this.lblCombustivel = new System.Windows.Forms.Label();
            this.rtbObservacao = new System.Windows.Forms.RichTextBox();
            this.lblModelo = new System.Windows.Forms.Label();
            this.lblProprietario = new System.Windows.Forms.Label();
            this.lblPlaca = new System.Windows.Forms.Label();
            this.lblObservacao = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(475, 369);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 119);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 39;
            this.pictureBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::Tela_principal.Properties.Resources.magnifier_glass_icon_icons_com_71148;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(547, 49);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(28, 23);
            this.button2.TabIndex = 38;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(76, 51);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(465, 20);
            this.textBox1.TabIndex = 37;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.BackgroundImage = global::Tela_principal.Properties.Resources.dwelling_house_318_1861;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(475, 496);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(66, 26);
            this.button1.TabIndex = 36;
            this.button1.Text = "Alterar";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.cnpj,
            this.descricao,
            this.Column1,
            this.Column2,
            this.excluir,
            this.alterar});
            this.dataGridView1.Location = new System.Drawing.Point(12, 87);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(620, 176);
            this.dataGridView1.TabIndex = 35;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.HeaderText = "Proprietario";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // cnpj
            // 
            this.cnpj.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cnpj.HeaderText = "Placa";
            this.cnpj.Name = "cnpj";
            this.cnpj.ReadOnly = true;
            // 
            // descricao
            // 
            this.descricao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.descricao.HeaderText = "Modelo";
            this.descricao.Name = "descricao";
            this.descricao.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Ano";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Combustivel";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // excluir
            // 
            this.excluir.HeaderText = "";
            this.excluir.Image = global::Tela_principal.Properties.Resources.edit;
            this.excluir.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.excluir.Name = "excluir";
            this.excluir.ReadOnly = true;
            this.excluir.Width = 40;
            // 
            // alterar
            // 
            this.alterar.HeaderText = "";
            this.alterar.Image = global::Tela_principal.Properties.Resources.trash;
            this.alterar.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.alterar.Name = "alterar";
            this.alterar.ReadOnly = true;
            this.alterar.Width = 40;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.BackgroundImage = global::Tela_principal.Properties.Resources.dwelling_house_318_1861;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(566, 496);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(66, 26);
            this.button7.TabIndex = 32;
            this.button7.Text = "Cancelar";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Modern No. 20", 30F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(198, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(149, 41);
            this.label7.TabIndex = 34;
            this.label7.Text = "Veiculo";
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.Location = new System.Drawing.Point(577, -18);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(17, 17);
            this.lblFechar.TabIndex = 33;
            this.lblFechar.Text = "X";
            // 
            // cboProprietário
            // 
            this.cboProprietário.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProprietário.FormattingEnabled = true;
            this.cboProprietário.Location = new System.Drawing.Point(78, 283);
            this.cboProprietário.Name = "cboProprietário";
            this.cboProprietário.Size = new System.Drawing.Size(121, 21);
            this.cboProprietário.TabIndex = 40;
            // 
            // txtPlaca
            // 
            this.txtPlaca.Location = new System.Drawing.Point(78, 310);
            this.txtPlaca.Mask = "aaa-0000";
            this.txtPlaca.Name = "txtPlaca";
            this.txtPlaca.Size = new System.Drawing.Size(59, 20);
            this.txtPlaca.TabIndex = 41;
            // 
            // cboModelo
            // 
            this.cboModelo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboModelo.FormattingEnabled = true;
            this.cboModelo.Location = new System.Drawing.Point(76, 339);
            this.cboModelo.Name = "cboModelo";
            this.cboModelo.Size = new System.Drawing.Size(121, 21);
            this.cboModelo.TabIndex = 42;
            // 
            // cboAnoFabricacao
            // 
            this.cboAnoFabricacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAnoFabricacao.FormattingEnabled = true;
            this.cboAnoFabricacao.Location = new System.Drawing.Point(479, 280);
            this.cboAnoFabricacao.Name = "cboAnoFabricacao";
            this.cboAnoFabricacao.Size = new System.Drawing.Size(82, 21);
            this.cboAnoFabricacao.TabIndex = 46;
            // 
            // lblAnoFabricacao
            // 
            this.lblAnoFabricacao.AutoSize = true;
            this.lblAnoFabricacao.Location = new System.Drawing.Point(410, 278);
            this.lblAnoFabricacao.Name = "lblAnoFabricacao";
            this.lblAnoFabricacao.Size = new System.Drawing.Size(63, 26);
            this.lblAnoFabricacao.TabIndex = 54;
            this.lblAnoFabricacao.Text = "Ano de \r\nFabricação:\r\n";
            // 
            // txtCor
            // 
            this.txtCor.Location = new System.Drawing.Point(278, 340);
            this.txtCor.Name = "txtCor";
            this.txtCor.Size = new System.Drawing.Size(101, 20);
            this.txtCor.TabIndex = 45;
            // 
            // lblCor
            // 
            this.lblCor.AutoSize = true;
            this.lblCor.Location = new System.Drawing.Point(246, 336);
            this.lblCor.Name = "lblCor";
            this.lblCor.Size = new System.Drawing.Size(26, 13);
            this.lblCor.TabIndex = 53;
            this.lblCor.Text = "Cor:";
            // 
            // txtOdometro
            // 
            this.txtOdometro.Location = new System.Drawing.Point(278, 310);
            this.txtOdometro.Name = "txtOdometro";
            this.txtOdometro.Size = new System.Drawing.Size(101, 20);
            this.txtOdometro.TabIndex = 44;
            // 
            // lblOdometro
            // 
            this.lblOdometro.AutoSize = true;
            this.lblOdometro.Location = new System.Drawing.Point(216, 310);
            this.lblOdometro.Name = "lblOdometro";
            this.lblOdometro.Size = new System.Drawing.Size(56, 13);
            this.lblOdometro.TabIndex = 52;
            this.lblOdometro.Text = "Odômetro:";
            // 
            // cboCombustivel
            // 
            this.cboCombustivel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCombustivel.FormattingEnabled = true;
            this.cboCombustivel.Location = new System.Drawing.Point(278, 278);
            this.cboCombustivel.Name = "cboCombustivel";
            this.cboCombustivel.Size = new System.Drawing.Size(121, 21);
            this.cboCombustivel.TabIndex = 43;
            // 
            // lblCombustivel
            // 
            this.lblCombustivel.AutoSize = true;
            this.lblCombustivel.Location = new System.Drawing.Point(205, 286);
            this.lblCombustivel.Name = "lblCombustivel";
            this.lblCombustivel.Size = new System.Drawing.Size(67, 13);
            this.lblCombustivel.TabIndex = 51;
            this.lblCombustivel.Text = "Combustivel:";
            // 
            // rtbObservacao
            // 
            this.rtbObservacao.Location = new System.Drawing.Point(33, 405);
            this.rtbObservacao.Name = "rtbObservacao";
            this.rtbObservacao.Size = new System.Drawing.Size(436, 96);
            this.rtbObservacao.TabIndex = 47;
            this.rtbObservacao.Text = "";
            // 
            // lblModelo
            // 
            this.lblModelo.AutoSize = true;
            this.lblModelo.Location = new System.Drawing.Point(27, 343);
            this.lblModelo.Name = "lblModelo";
            this.lblModelo.Size = new System.Drawing.Size(45, 13);
            this.lblModelo.TabIndex = 50;
            this.lblModelo.Text = "Modelo:";
            // 
            // lblProprietario
            // 
            this.lblProprietario.AutoSize = true;
            this.lblProprietario.Location = new System.Drawing.Point(9, 288);
            this.lblProprietario.Name = "lblProprietario";
            this.lblProprietario.Size = new System.Drawing.Size(63, 13);
            this.lblProprietario.TabIndex = 48;
            this.lblProprietario.Text = "Proprietário:";
            // 
            // lblPlaca
            // 
            this.lblPlaca.AutoSize = true;
            this.lblPlaca.Location = new System.Drawing.Point(30, 309);
            this.lblPlaca.Name = "lblPlaca";
            this.lblPlaca.Size = new System.Drawing.Size(37, 13);
            this.lblPlaca.TabIndex = 49;
            this.lblPlaca.Text = "Placa:";
            // 
            // lblObservacao
            // 
            this.lblObservacao.AutoSize = true;
            this.lblObservacao.Location = new System.Drawing.Point(30, 389);
            this.lblObservacao.Name = "lblObservacao";
            this.lblObservacao.Size = new System.Drawing.Size(73, 13);
            this.lblObservacao.TabIndex = 55;
            this.lblObservacao.Text = "Observações:";
            // 
            // frm_consu_alte_etc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Moccasin;
            this.ClientSize = new System.Drawing.Size(644, 534);
            this.Controls.Add(this.lblObservacao);
            this.Controls.Add(this.cboProprietário);
            this.Controls.Add(this.txtPlaca);
            this.Controls.Add(this.cboModelo);
            this.Controls.Add(this.cboAnoFabricacao);
            this.Controls.Add(this.lblAnoFabricacao);
            this.Controls.Add(this.txtCor);
            this.Controls.Add(this.lblCor);
            this.Controls.Add(this.txtOdometro);
            this.Controls.Add(this.lblOdometro);
            this.Controls.Add(this.cboCombustivel);
            this.Controls.Add(this.lblCombustivel);
            this.Controls.Add(this.rtbObservacao);
            this.Controls.Add(this.lblModelo);
            this.Controls.Add(this.lblProprietario);
            this.Controls.Add(this.lblPlaca);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblFechar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_consu_alte_etc";
            this.Text = "frm_consu_alte_etc";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn cnpj;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewImageColumn excluir;
        private System.Windows.Forms.DataGridViewImageColumn alterar;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.ComboBox cboProprietário;
        private System.Windows.Forms.MaskedTextBox txtPlaca;
        private System.Windows.Forms.ComboBox cboModelo;
        private System.Windows.Forms.ComboBox cboAnoFabricacao;
        private System.Windows.Forms.Label lblAnoFabricacao;
        private System.Windows.Forms.TextBox txtCor;
        private System.Windows.Forms.Label lblCor;
        private System.Windows.Forms.TextBox txtOdometro;
        private System.Windows.Forms.Label lblOdometro;
        private System.Windows.Forms.ComboBox cboCombustivel;
        private System.Windows.Forms.Label lblCombustivel;
        private System.Windows.Forms.RichTextBox rtbObservacao;
        private System.Windows.Forms.Label lblModelo;
        private System.Windows.Forms.Label lblProprietario;
        private System.Windows.Forms.Label lblPlaca;
        private System.Windows.Forms.Label lblObservacao;
    }
}