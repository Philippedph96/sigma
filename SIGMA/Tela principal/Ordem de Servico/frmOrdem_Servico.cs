﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tela_principal.Ordem_de_Servico
{
    public partial class frmOrdem_Servico : Form
    {
        public frmOrdem_Servico()
        {
            InitializeComponent();
        }
        void CarregarGrid()
        {
          
           Database_Ordem_Servico db = new Database_Ordem_Servico();

            List<DTO_Ordem_Servico> lista = db.Consultar();

            dgvnaosei.AutoGenerateColumns = false;
            dgvnaosei.DataSource = lista;
        }
        void CarregarGridFiltro()
        {
            Database_Ordem_Servico db = new Database_Ordem_Servico();
            List<DTO_Ordem_Servico> lista = db.ConsultarFiltro(dtpentrada.Value,dtpsaida.Value);

            dgvnaosei.AutoGenerateColumns = false;
            dgvnaosei.DataSource = lista;

        }
        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar está tela?",
                                                   "Projeto Sigma",
                                                   MessageBoxButtons.YesNo,
                                                   MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
                this.Close();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        DateTime nada;
        private void btnok_Click(object sender, EventArgs e)
        {
            if (dtpentrada.Value == nada && dtpsaida.Value == nada && txtodometro.Text == string.Empty )
            {
                CarregarGrid();
            }
            else
            {
                CarregarGridFiltro();
            }
        }
    }
}
