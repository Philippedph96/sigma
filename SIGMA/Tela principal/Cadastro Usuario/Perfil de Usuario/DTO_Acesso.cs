﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Login
{
    class DTO_Acesso
    {

        public int ID  { get; set; } //OK
        public int ID_Usuario { get; set; }

        public bool Acesso_Menu { get; set; }
        public bool Inserir_Dados { get; set; }
        public bool Alterar_Dados { get; set; }
        public bool Excluir_Dados { get; set; }


    }
}
