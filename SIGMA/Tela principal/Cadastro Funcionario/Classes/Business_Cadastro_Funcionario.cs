﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_funcionario
{
    class Business_Funcionario
    {
        public int Salvar(DTO_Funcionario dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }
            if (dto.DatadeNascimento == null)
            {
                throw new ArgumentException("Data de nascimento é obrigatório!");
            }
            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }
            if (dto.Endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório!");
            }
            if (dto.Complemento == string.Empty)
            {
                throw new ArgumentException("Complemento é obrigatório!");
            }
            if (dto.Foto == string.Empty)
            {
                throw new ArgumentException("Foto é obrigatório!");
            }
            if (dto.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório!");
            }
            if (dto.UF == string.Empty)
            {
                throw new ArgumentException("UF é obrigatório!");
            }
            if (dto.Numero == 0)
            {
                throw new ArgumentException("Número da casa é obrigatório!");
            }
            string re = @"^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}$";
            Regex regex = new Regex(re);
            if (regex.IsMatch(dto.Email) == false)
            {
                throw new ArgumentException("Email inválido!");
            }
            if (dto.TelefoneResidencial == string.Empty)
            {
                throw new ArgumentException("Telefone residencial é obrigatório!");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório!");
            }
            if (dto.Celular == string.Empty)
            {
                throw new ArgumentException("Celular é obrigatório!");
            }
            if (dto.Cidade == string.Empty)
            {
                throw new ArgumentException("Cidade é obrigatório!");
            }

            Database_Funcionario db = new Database_Funcionario();
            return db.Salvar(dto);

        }
        public void Alterar(DTO_Funcionario dto)
        {
            Database_Funcionario db = new Database_Funcionario();
            db.Alterar(dto);
        }
        public void Remover (int dto)
        {
            Database_Funcionario db = new Database_Funcionario();
            db.Remover(dto);
        }
        public List<DTO_Funcionario> Listar()
        {
            Database_Funcionario db = new Database_Funcionario();
            List<DTO_Funcionario> lista = db.Listar();

            return lista;
        }
    }
}
