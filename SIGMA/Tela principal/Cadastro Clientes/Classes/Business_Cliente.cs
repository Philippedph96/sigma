﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_Clientes
{
    class Business_Cliente
    {
        public  int Salvar(DTO_Cliente dto) {

            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }
            if (dto.Profissao == string.Empty)
            {
                throw new ArgumentException("Profissão é obrigatório!");
            }
            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório!");
            }
            if (dto.CNPJ == string.Empty)
            {
                throw new ArgumentException("CNPJ é obrigatório!");
            }
            if (dto.RG == string.Empty)
            {
                throw new ArgumentException("RG é obrigatório!");
            }
            if (dto.DatadeNascimento == null)
            {
                throw new ArgumentException("Data de nascimento é obrigatório!");
            }
            if (dto.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório!");
            }
            if (dto.UF== string.Empty)
            {
                throw new ArgumentException("UF é obrigatório!");
            }
            if (dto.Endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório!");
            }
            if (dto.Telefone_Residencial == string.Empty)
            {
                throw new ArgumentException("Telefone residencial é obrigatório!");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório!");
            }
            if (dto.Celular == string.Empty)
            {
                throw new ArgumentException("Celular é obrigatório!");
            }
            if (dto.Tipo_Carta == string.Empty)
            {
                throw new ArgumentException("Tipo de carta é obrigatório!");
            }
            string re = @"^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}$";
            Regex regex = new Regex(re);
            if (regex.IsMatch(dto.Email) == false)
            {
                throw new ArgumentException("Email inválido!");
            }
            if (dto.Complemento == string.Empty)
            {
                throw new ArgumentException("Complemento é obrigatório!");
            }
            if (dto.Numero == 0)
            {
                throw new ArgumentException("Número é obrigatório!");
            }
            
            Database_Cliente db = new Database_Cliente();
            return db.Salvar(dto);
        }

        public void Alterar(DTO_Cliente dto)
        {
            Database_Cliente db = new Database_Cliente();
            db.Alterar(dto);
        }

        public void Remover(int dto)
        {
            Database_Cliente db = new Database_Cliente();
            db.Remover(dto);
        }
        public List<DTO_Cliente> Listar()
        {
            Database_Cliente db = new Database_Cliente();
            List<DTO_Cliente> lista = db.Listar();

            return lista;
        }

    }
}
