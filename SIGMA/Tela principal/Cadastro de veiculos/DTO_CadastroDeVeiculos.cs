﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_veiculos
{
    class DTO_CadastroDeVeiculos
    {
        public int ID { get; set; } //ok
        public string Placa { get; set; } //ok
        public string Combustivel { get; set; } //ok
        public int Odometro { get; set; } //ok
        public string Cor { get; set; } //ok
        public DateTime Ano { get; set; } //ok
        public string Observaçoes { get; set; } //ok
        //proprietario e modelo , outra tabela 

    }
}
