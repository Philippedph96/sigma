﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Ordem_de_Servico
{
    class DTO_Ordem_Servico
    {
        public int ID { get; set; }
        public string placa { get; set; }
        public DateTime DataDeEntrega { get; set; } //ok
        public DateTime DataDesaida { get; set; } //ok
        public string Observacoes { get; set; } //ok
        public double  Valor { get; set; }
        public string Funcionario { get; set; }



        
    }
}
