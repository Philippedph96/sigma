﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.LModelodoVeiculo;

namespace Tela_principal
{
    public partial class frmModelo : Form
    {
        public frmModelo()
        {
            InitializeComponent();
        }

        DTO_Modelo dto = new DTO_Modelo();

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database_Modelo db = new Database_Modelo();
                List<DTO_Modelo> list = db.Listar();

                dvgmodelo.AutoGenerateColumns = false;
                dvgmodelo.DataSource = list;

                lblRef.Text = dvgmodelo.Rows.Count.ToString();
            }
            catch (ArgumentException erro)
            {
                MessageBox.Show("ERRO!/n " + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!/n " + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

            

           // tem que contar quantos registros e colocar na label 4

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
            this.Close();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar está tela?",
                                                  "Projeto Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
                this.Close();
            }
        }
    }
}
