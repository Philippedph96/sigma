﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tela_principal.Orçamento
{
    public partial class frm_FinalizacaodeOrcamento : Form
    {
        public frm_FinalizacaodeOrcamento()
        {
            InitializeComponent();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {
            DialogResult dialogue = MessageBox.Show("Deseja Realmente fechar este programa?",
                                                    "Programa Sigma",
                                                    MessageBoxButtons.YesNo,
                                                    MessageBoxIcon.Question);
            if (dialogue == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
