﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tela_principal.Sub_Telas
{
    public partial class frmItensDeServicoOrcado : Form
    {
        public frmItensDeServicoOrcado()
        {
            InitializeComponent();
        }

        private void label23_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar está tela?",
                                                  "Projeto Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
                this.Close();
            }
        }
    }
}
