﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_Usuario;

namespace Tela_principal
{
    public partial class frmCadastro_Usuario : Form
    {
        public frmCadastro_Usuario()
        {
            InitializeComponent();
            CarregarDepartamentos();

            List<string> perfil = new List<string>();
            perfil.Add("Administrador");
            perfil.Add("Funcionário");

            cboPerfil.DataSource = perfil;
        }

       void CarregarDepartamentos ()
        {
            Business_Departamento b = new Business_Departamento();
            List<DTO_Departamento> lista = b.Listar();

            cboDepartamento.ValueMember = nameof(DTO_Departamento.ID);
            cboDepartamento.DisplayMember = nameof(DTO_Departamento.Departamento);
            cboDepartamento.DataSource = lista;
       }

        public void Automagicamente()
        {
            try
            {

            DTO_Departamento departamento = cboDepartamento.SelectedItem as DTO_Departamento;

            DTO_Usuarios dto = new DTO_Usuarios();
            dto.ID_Departamento = departamento.ID; //Relacionamento Completo!!
            dto.Nome_User = txtNome.Text;
            dto.Usuario = txtUsuario.Text;
            dto.Perfil = cboPerfil.SelectedItem.ToString();
            dto.Obeservacao = txtObservacao.Text;
            bool n = rdnNao.Checked;
            if (n == true)
            {
                dto.Checar = n;
            }
            else
            {
                dto.Checar = rdnSim.Checked;
            }
            if (txtSenha.Text != txtConfirmarSenha.Text)
            {
                MessageBox.Show("Senhas incorretas!",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                
            }
            else
            {
                string senha = txtSenha.Text;
                SHA256Cript crip = new SHA256Cript();
                dto.Senha = crip.Criptografar(senha);
            }

            Business_Usuario db = new Business_Usuario();
            db.Salvar(dto);
            MessageBox.Show("Usuário cadastrado com sucesso",
                            "Sigma",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }
        public void Abracadabra()
        {
            try
            {
                DTO_Departamento departamento = cboDepartamento.SelectedItem as DTO_Departamento;

                DTO_Usuarios dto = new DTO_Usuarios();
                dto.ID_Departamento = departamento.ID; //Relacionamento Completo!!
                dto.Nome_User = txtNome.Text;
                dto.Usuario = txtUsuario.Text;
                dto.Obeservacao = txtObservacao.Text;
                bool n = rdnNao.Checked;
                if (n == true)
                {
                    dto.Checar = n;
                }
                else
                {
                    dto.Checar = rdnSim.Checked;
                }
                if (txtSenha.Text != txtConfirmarSenha.Text)
                {
                    MessageBox.Show("Senhas incorretas!",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);

                }
                else
                {
                    string senha = txtSenha.Text;
                    SHA256Cript crip = new SHA256Cript();
                    dto.Senha = crip.Criptografar(senha);
                }

                Business_Usuario db = new Business_Usuario();
                db.Salvar(dto);
                MessageBox.Show("Usuário alterado com sucesso",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);

               
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                  "Sigma",
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Error);
            }
        }

        public void SHAZAM()
        {
            btnLocalizar.Enabled = true;
            cboDepartamento.Enabled = true;
            txtNome.Enabled = true;
            txtUsuario.Enabled = true;
            cboPerfil.Enabled = true;
            txtSenha.Enabled = true;
            txtConfirmarSenha.Enabled = true;
            rdnSim.Enabled = true;
            rdnNao.Enabled = true;
            txtObservacao.Enabled = true;
            btnSalvar.Enabled = true;
            btnExcluir.Enabled = true;
            btnCancelar.Enabled = true;
            btnAlterar.Enabled = true;
        }
        public void Desaparecer()
        {
            try
            {

                DialogResult dialog = MessageBox.Show("Deseja realmente fechar este programa?",
                                                            "Projeto Sigma",
                                                            MessageBoxButtons.YesNo,
                                                            MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    Application.Exit();
                }

            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }
        public void Alacazum()
        {
            try
            {

                DialogResult dialog = MessageBox.Show("Você realmente deseja deletar esse registro?",
                                "Sigma",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question);

                if (dialog == DialogResult.Yes)
                {
                    DTO_Usuarios dto = new DTO_Usuarios();
                    int id = dto.ID;

                    Business_Usuario db = new Business_Usuario();
                    db.Remover(id);
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }

        public void Magicamente()
        {
            DialogResult dialog = MessageBox.Show("Você tem certeza que deseja cancelar o cadastro?",
                                                  "Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                this.Hide();

                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
            this.Close();
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            SHAZAM();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Desaparecer();
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Não é possível encontrar usuários!", 
                            "Sigma",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Automagicamente();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Alacazum();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Magicamente();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Abracadabra();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar está tela?",
                                                  "Projeto Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
                this.Close();
            }
        }
    }
}
