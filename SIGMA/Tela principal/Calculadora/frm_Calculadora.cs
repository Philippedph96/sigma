﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tela_principal.Telas
{
    public partial class frmCalculadora : Form
    {
        public frmCalculadora()
        {
            InitializeComponent();
        }

        double numero1;
        double numero2;
        double resultado;
        string operador; //são os sinais
        bool operacao = true; //são os números.

        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar está tela?",
                                                  "Projeto Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
                this.Close();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            this.Close();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "0")
            {
                return;
            }
            else
            {
                textBox2.Text = textBox2.Text += 0;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (operacao)
            {
                textBox2.Text = textBox2.Text = "";
                textBox2.Text = textBox2.Text = "1";
                operacao = false;
            }
            else
            {
                textBox2.Text = textBox2.Text += "1";
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (operacao)
            {
                textBox2.Text = textBox2.Text = "";
                textBox2.Text = textBox2.Text = "2";
                operacao = false;
            }
            else
            {
                textBox2.Text = textBox2.Text += 2;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (operacao)
            {
                textBox2.Text = textBox2.Text = "";
                textBox2.Text = textBox2.Text = "3";
                operacao = false; //essa linha serve para colocar a operacao em falso para repetir o nº. Ex.: 3333.
            }
            else
            {
                textBox2.Text = textBox2.Text += 3;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (operacao)
            {
                textBox2.Text = textBox2.Text = "";
                textBox2.Text = textBox2.Text = "4";
                operacao = false;
            }
            else
            {
                textBox2.Text = textBox2.Text += 4;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (operacao)
            {
                textBox2.Text = textBox2.Text = "";
                textBox2.Text = textBox2.Text = "5";
                operacao = false;
            }
            else
            {
                textBox2.Text = textBox2.Text += 5;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (operacao)
            {
                textBox2.Text = textBox2.Text = "";
                textBox2.Text = textBox2.Text = "6";
                operacao = false;
            }
            else
            {
                textBox2.Text = textBox2.Text += 6;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (operacao)
            {
                textBox2.Text = textBox2.Text = "";
                textBox2.Text = textBox2.Text = "7";
                operacao = false;
            }
            else
            {
                textBox2.Text = textBox2.Text += 7;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (operacao)
            {
                textBox2.Text = textBox2.Text = "";
                textBox2.Text = textBox2.Text = "8";
                operacao = false;
            }
            else
            {
                textBox2.Text = textBox2.Text += 8;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (operacao)
            {
                textBox2.Text = textBox2.Text = "";
                textBox2.Text = textBox2.Text = "9";
                operacao = false;
            }
            else
            {
                textBox2.Text = textBox2.Text += 9;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
            textBox2.Text = textBox2.Text = "0";
        }

        private void button18_Click(object sender, EventArgs e)
        {
            numero1 = Convert.ToDouble(textBox2.Text);
            numero2 = Convert.ToDouble(textBox2.Text); //coloca-se o numero2 porque é o último a ficar na memória
            operacao = true;
            switch (operador)
            {
                case "+":
                    {
                        resultado = numero1 + numero2;
                        textBox2.Text = textBox2.ToString();
                        break;
                    }
                case "-":
                    {
                        resultado = numero1 - numero2;
                        textBox2.Text = textBox2.ToString();
                        break;
                    }
                case "*":
                    {
                        resultado = numero1 * numero2;
                        textBox2.Text = textBox2.ToString();
                        break;
                    }
                case "/":
                    {
                        resultado = numero1 / numero2;
                        textBox2.Text = textBox2.ToString();
                        break;
                    }
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            operador = "+";
            operacao = true;
            numero1 = Convert.ToDouble(textBox2.Text);
        }

        private void button20_Click(object sender, EventArgs e)
        {
            operador = "-";
            operacao = true;
            numero1 = Convert.ToDouble(textBox2.Text);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            operador = "*";
            operacao = true;
            numero1 = Convert.ToDouble(textBox2.Text);
        }

        private void button22_Click(object sender, EventArgs e)
        {
            operador = "/";
            operacao = true;
            numero1 = Convert.ToDouble(textBox2.Text);
        }
    }
}
